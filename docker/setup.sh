#!/bin/sh

## Replace values in the nginx's default.conf for proxy
envsubst '\$API_SERVER, \
\$FILE_SERVER, \
\$BNF_API_SERVER' \
< /etc/nginx/conf.d/default.conf.template \
> /etc/nginx/conf.d/default.conf

## Replace values in the template config.json
envsubst '\$OAUTH_ISSUER, \
\$OAUTH_CLIENT_ID, \
\$OAUTH_SILENT_RENEW, \
\$OAUTH_RENEW_TIME_BEFORE_EXPIRE \
\$FULL_PROJECT_VERSION, \
\$GLOBAL_PROJECT_VERSION, \
\$URL_SCANNER, \
\$CI_PIPELINE_ID, \
\$CI_JOB_ID, \
\$CI_COMMIT_SHA' \
< /usr/share/nginx/html/assets/config/config.json.template \
> /usr/share/nginx/html/assets/config/config.json
