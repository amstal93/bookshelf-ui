import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthModule, OidcConfigService, OidcSecurityService, OpenIdConfiguration} from 'angular-auth-oidc-client';
import {OidcSecurityServiceMock} from "./oidc-security.service.mock";
import {ConfigurationServiceMock} from "./configuration.service.mock";

@NgModule({
  imports: [
    CommonModule,
    AuthModule.forRoot({
      config: {
        authority: 'test',
        redirectUrl: 'test',
        clientId: 'test'
      } as OpenIdConfiguration
    }),
  ],
  providers: [
    {provide: OidcSecurityService, useClass: OidcSecurityServiceMock},
    {provide: OidcConfigService, useClass: ConfigurationServiceMock},
  ]
})
export class AngularAuthOidcClientTestingModule {
}
