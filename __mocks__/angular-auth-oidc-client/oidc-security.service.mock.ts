import {of} from 'rxjs';

export class OidcSecurityServiceMock {
  getToken() {
    return 'some_token_eVbnasdQ324';
  }

  getIdToken() {
    return 'some_token_eVbnasdQ324';
  }

  logoff() {
    return;
  }

  login() {
    return;
  }

  checkAuth() {
    return of(true);
  }

  authorize() {
    return;
  }

  get isAuthenticated$() {
    return of({
      isAuthenticated: true,
      allConfigsAuthenticated: []
    });
  }

  // similarly mock other methods "oidcSecurityService" as per the component requirement

}
