import {TestBed, waitForAsync} from '@angular/core/testing';

import {AuthGuard} from './auth.guard';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {
  AngularAuthOidcClientTestingModule
} from '../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {ActivatedRouteSnapshot} from "@angular/router";

describe('AuthGuard', () => {
  let httpTestingController: HttpTestingController;
  let guard: AuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        AngularAuthOidcClientTestingModule
      ],
      providers: [
        AuthGuard
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    guard = TestBed.inject(AuthGuard);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should instantiate', () => {
      expect(guard).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should create', waitForAsync(() => {
      guard.canActivateChild(new ActivatedRouteSnapshot(), undefined)
        .subscribe(value => expect(value).toBeTruthy());
    }));
  });
});
