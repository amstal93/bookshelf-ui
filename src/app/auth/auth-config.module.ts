import {NgModule} from '@angular/core';
import {
  AuthInterceptor,
  AuthModule,
  LogLevel,
  OpenIdConfiguration,
  StsConfigHttpLoader,
  StsConfigLoader
} from 'angular-auth-oidc-client';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {ConfigService} from "../core/services/config.service";
import {environment} from "../../environments/environment";
import {AppConfig} from "../core/model/app-config";

export function httpLoaderFactory(httpClient: HttpClient, configService: ConfigService) {
  const config$ = httpClient.get<AppConfig>(`./assets/config/config.json`).pipe(
    tap(customConfig => configService.setAppConfig(customConfig)),
    map<AppConfig, OpenIdConfiguration>((customConfig) => ({
        configId: 'config',
        authority: customConfig.oauth.stsServer,
        redirectUrl: window.location.origin,
        postLogoutRedirectUri: window.location.origin,
        clientId: customConfig.oauth.clientId,
        scope: 'openid profile email offline_access',
        responseType: 'code',
        silentRenew: customConfig.oauth.silentRenew,
        useRefreshToken: true,
        ignoreNonceAfterRefresh: true,
        silentRenewUrl: `${window.location.origin}/silent-renew.html`,
        renewTimeBeforeTokenExpiresInSeconds: customConfig.oauth.renewTimeBeforeTokenExpiresInSeconds,
        logLevel: environment.production ? LogLevel.None : LogLevel.Warn,
        secureRoutes: ['/api'],
      }) as OpenIdConfiguration
    )
  );

  return new StsConfigHttpLoader(config$);
}

@NgModule({
  imports: [
    HttpClientModule,
    AuthModule.forRoot({
      loader: {
        provide: StsConfigLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient, ConfigService],
      }
    })
  ],
  exports: [AuthModule],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ]
})
export class AuthConfigModule {
}
