import {Component, Input} from '@angular/core';
import {Series} from '../../core/model/series';

@Component({
  selector: 'app-title-display',
  template: `
      <span [ngClass]="class">
    <ng-container class="mat-h2" *ngIf="series !== undefined && !series.oneShot">
      {{series.displayName}}
        <ng-container class="mat-h2" *ngIf="tome !== null && tome !== undefined">
          (T{{tome}})
        </ng-container>
    </ng-container>
          {{title}}
    </span>
      <span *ngIf="isbn !== null" class="mat-small"> {{isbn}}</span>
  `
})
export class TitleDisplayComponent {

  @Input() title: string;
  @Input() tome: string;
  @Input() series: Series;
  @Input() isbn: string = null;
  @Input() class: string;

}
