import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogConfirmDataModel {
  message: string;
  title: string;
  question: string;
}

@Component({
  selector: 'app-dialog-confirm',
  template:  `
    <h1 mat-dialog-title>{{ data.title }}</h1>
    <div mat-dialog-content>
      <p>{{ data.message }}</p>
      <p>{{ data.question }}</p>
    </div>
    <mat-dialog-actions align="end">
      <button mat-button mat-dialog-close>{{ 'GENERIC.DIALOG.CONFIRM.KO' | translate }}</button>
      <button mat-button [mat-dialog-close]="true" cdkFocusInitial> {{ 'GENERIC.DIALOG.CONFIRM.OK' | translate }}</button>
    </mat-dialog-actions>
  `
})
export class DialogConfirmComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogConfirmDataModel
  ) {
  }
}
