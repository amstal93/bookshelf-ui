import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {LoaderBarComponent} from './loader-bar.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {coreServiceMock} from '../../core/services/__mocks__/core.service';
import {CoreService} from '../../core/services/core.service';

describe('LoaderBarComponent', () => {
  let component: LoaderBarComponent;
  let fixture: ComponentFixture<LoaderBarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LoaderBarComponent ],
      imports: [
        MatProgressBarModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});
