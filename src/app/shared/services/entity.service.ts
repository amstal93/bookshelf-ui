import {BehaviorSubject, Observable, throwError} from "rxjs";
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {CoreService} from "../../core/services/core.service";
import {catchError, tap} from "rxjs/operators";
import {idType, RestEntity} from "../../core/model/rest-entity";
import {SortOrder} from "../../core/model/sort-order.enum";
import {Utils} from "../utils";
import {InjectionToken} from "@angular/core";
import {AdministrationService} from "../../administration/services/administration.service";
import {KeyValue} from "../../core/model/key-value";

export const SEARCH_PARAM = new InjectionToken<AdministrationService<any>>('SearchParam');

export interface SearchCriteria {
  name: string;
  operation: string;
  value: string | number;
  or?: boolean
}

export interface SearchPagination {
  page?: number;
  size?: number;
}

export class SearchPaginationImpl {
  constructor(
    public page: number = 0,
    public size: number = 5
  ) {
  }
}

export interface SearchOptions {
  pagination?: SearchPagination | null;
  withLoader?: boolean;
  updateList?: boolean;
  direction: SortOrder;
  sort?: string;
}

export class SearchOptionsImpl {

  constructor(
    public withLoader: boolean = true,
    public updateList: boolean = true,
    public direction: SortOrder = SortOrder.ASC
  ) {
  }
}

export class EntityService<T extends RestEntity> {

  protected apiEndpoints = [''];

  get list$(): Observable<T[]> {
    return this.list.asObservable();
  }

  protected constructor(
    protected http: HttpClient,
    protected coreService: CoreService,
    ...apiEndpoints: string[]
  ) {
    this.apiEndpoints = apiEndpoints;
  }

  protected list: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);

  protected getId(t: T): idType {
    return t.id;
  }

  getAll(...ids: idType[]): Observable<T[]> {
    this.coreService.updateLoadingState(true);
    return this.http.get<T[]>(Utils.buildUrl(this.apiEndpoints, ...ids)).pipe(
      tap(res => this.updateList(res)),
      catchError(err => this.manageError(err))
    );
  }

  getById(...ids: idType[]): Observable<T> {
    this.coreService.updateLoadingState(true);
    return this.http.get<T>(Utils.buildUrl(this.apiEndpoints, ...ids)).pipe(
      tap(() => this.coreService.updateLoadingState(false)),
      catchError(err => this.manageError(err))
    );
  }

  searchWithOption(
    searchCriteriaList: SearchCriteria[] = [],
    searchOptions: SearchOptions = new SearchOptionsImpl()
  ): Observable<HttpResponse<T[]>>  {
    this.coreService.updateLoadingState(searchOptions.withLoader);
    let params = new HttpParams();

    const mappedSearchCriteria = EntityService.mapSearchCriteria(searchCriteriaList)
    if (mappedSearchCriteria !== '') {
      params = params.append('search', mappedSearchCriteria);
    }
    if (searchOptions.pagination) {
      params = params.append('page', searchOptions.pagination.page)
        .append('size', searchOptions.pagination.size)
    }
    if (searchOptions.sort) {
      params = params.append('sort', searchOptions.sort)
    }
    params = params.append('direction', searchOptions.direction)
      .append('allResults', !searchOptions.pagination);
    return this.http.get<T[]>(Utils.buildUrl(this.apiEndpoints), {observe: "response", params})
      .pipe(
        tap((res: HttpResponse<T[]>) => {
          if (searchOptions.updateList) {
            this.updateList(res.body);
          } else {
            this.coreService.updateLoadingState(false);
          }
        }),
        catchError(err => this.manageError(err))
      );
  }

  search(
    searchCriteriaList: SearchCriteria[] = [],
    page: number = 0,
    size: number = 5,
    withLoader: boolean = true,
    updateList: boolean = true,
    direction: SortOrder = SortOrder.ASC,
    sort?: string,
  ): Observable<HttpResponse<T[]>> {
    return this.searchWithOption(
      searchCriteriaList,
      {
        pagination: {page, size},
        updateList,
        withLoader,
        direction,
        sort
      })
  }

  searchAutocomplete(
    searchCriteriaList: SearchCriteria[] = [],
    additionalParams: KeyValue[] = []
  ): Observable<HttpResponse<T[]>> {
    let params = new HttpParams();

    const mappedSearchCriteria = EntityService.mapSearchCriteria(searchCriteriaList)
    if (mappedSearchCriteria !== '') {
      params = params.append('search', mappedSearchCriteria);
    }

    additionalParams.forEach( param => params = params.append(param.key, param.value))

    return this.http.get<T[]>(Utils.buildUrl([...this.apiEndpoints, 'autocomplete']), {observe: "response", params})
      .pipe(
        tap((res: HttpResponse<T[]>) => this.updateList(res.body)),
        catchError(err => this.manageError(err))
      );
  }

  updateList(res: T[], append: boolean = false) {
    if (append) {
      this.list.next([...this.list.value, ...res])
    } else {
      this.list.next(res)
    }
    this.coreService.updateLoadingState(false);
  }

  protected manageError(err) {
    console.error('an error occured!', err);
    this.coreService.updateLoadingState(false);
    return throwError(() => new Error(err));
  }

  public static mapSearchCriteria(searchCriteriaList: SearchCriteria[]): string {
    return searchCriteriaList
      .filter(searchCriteria => searchCriteria.value !== '**' && searchCriteria.value !== '*' && searchCriteria.value !== '' )
      .map(searchCriteria => {
        const sanitizedValue = `${searchCriteria.value}`.replace(/\+/g,'%2B')
        return `${searchCriteria.or ? '\'' : ''}${searchCriteria.name}${searchCriteria.operation}${sanitizedValue}`
      })
      .join('|');
  }
}
