import {Subject} from "rxjs";

export const scrollEventServiceMocK = {
  emitBottomReached: jest.fn(),
  bottomReached$: new Subject<void>(),
  toggleScroll: jest.fn()
}
