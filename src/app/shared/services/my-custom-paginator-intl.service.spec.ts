import {TestBed} from '@angular/core/testing';

import {MyCustomPaginatorIntlService} from './my-custom-paginator-intl.service';
import {NgxTranslateTestingModule} from "../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {TranslateService} from "@ngx-translate/core";

describe('MyCustomPaginatorIntlService', () => {
  let service: MyCustomPaginatorIntlService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxTranslateTestingModule
      ],
      providers: [
        {
          provide: MyCustomPaginatorIntlService,
          useFactory: (translate: TranslateService) =>  new MyCustomPaginatorIntlService('test', translate), deps: [TranslateService]
        }
      ],
    });
    service = TestBed.inject(MyCustomPaginatorIntlService);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('should return range label for length 0', () => {
      jest.clearAllMocks();
      const spy = jest.spyOn(service[`translate`], `instant`)
      expect(service.getRangeLabel(1, 2, 0)).toStrictEqual('PAGINATOR.PAGE_OF');
      expect(spy).toHaveBeenNthCalledWith(1, 'PAGINATOR.PAGE_OF', {page: 2, amountPages: 1})
    });
    test('should return range label for length > 0', () => {
      jest.clearAllMocks();
      const spy = jest.spyOn(service[`translate`], `instant`)
      expect(service.getRangeLabel(1, 2, 4)).toStrictEqual('PAGINATOR.PAGE_OF');
      expect(spy).toHaveBeenNthCalledWith(1, 'PAGINATOR.PAGE_OF', {page: 2, amountPages: 2})
    });
  });
});
