import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {ResolvedDataPaginated} from "../resolvers/list-resolver";
import {environment} from "../../../environments/environment";

export interface NewPageEvent {
  page: number;
  size: number;
}

@Injectable({
  providedIn: 'root',
})
export class PaginationService {

  static pageSizes = [environment.DEFAULT_PAGE_SIZE, 25, 50, 100];
  private totalCount: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private totalPage: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private pageSize: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private currentPage: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private filterStr: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private newPageEvent: BehaviorSubject<NewPageEvent> = new BehaviorSubject<NewPageEvent>(null);

  get totalCount$(): Observable<number> {
    return this.totalCount.asObservable();
  }

  get pageSize$(): Observable<number> {
    return this.pageSize.asObservable();
  }

  get currentPage$(): Observable<number> {
    return this.currentPage.asObservable();
  }

  get filterStr$(): Observable<string> {
    return this.filterStr.asObservable();
  }

  get newPageEvent$(): Observable<NewPageEvent> {
    return this.newPageEvent.asObservable();
  }

  updatePagination(resolvedData: ResolvedDataPaginated<any>) {
    this.totalCount.next(resolvedData.pagination.totalCount);
    this.currentPage.next(resolvedData.pagination.pageNumber);
    this.pageSize.next(resolvedData.pagination.pageSize);
    this.totalPage.next(resolvedData.pagination.totalPage);
  }

  updatePaginationAndFilter(resolvedData: ResolvedDataPaginated<any>) {
    this.updatePagination(resolvedData);
    this.filterStr.next(resolvedData.filter);
  }

  onNewPage(page: NewPageEvent): void {
    this.newPageEvent.next(page)
  }

  getNextPage(): null | number {
    return this.currentPage.value < this.totalPage.value - 1 ? this.currentPage.value + 1 : null;
  }

  getCurrentPagination(): { filterStr: string, pageSize: number, currentPage: number } {
    return { filterStr: this.filterStr.value, pageSize: this.pageSize.value, currentPage: this.currentPage.value}
  }
}
