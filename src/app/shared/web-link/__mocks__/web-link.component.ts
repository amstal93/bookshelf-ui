import {Component, Input} from '@angular/core';
import {WebLink} from '../../../core/model/web-link';

@Component({
  selector: 'app-web-link',
  template: '<div> Web link Display</div>'
})
export class MockWebLinkComponent {

  @Input()
  webLink: WebLink;
}
