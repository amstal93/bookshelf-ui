import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WebLinkComponent} from './web-link.component';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';

describe('WebLinkComponent', () => {
  let component: WebLinkComponent;
  let fixture: ComponentFixture<WebLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebLinkComponent ],
      imports: [
        FontAwesomeTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should set twitter icon', () => {
      component.webLink = {value: 'toto', type: 'TWITTER'}

      expect(component.href).toEqual('https://twitter.com/toto');
      expect(component.icon).toStrictEqual(['fab', 'twitter']);
    });
    test('should set facebook icon', () => {
      component.webLink = {value: 'toto', type: 'FACEBOOK'}

      expect(component.href).toEqual('https://www.facebook.com/toto');
      expect(component.icon).toStrictEqual(['fab', 'facebook']);
    });
    test('should set instagram icon', () => {
      component.webLink = {value: 'toto', type: 'INSTAGRAM'}

      expect(component.href).toEqual('https://www.instagram.com/toto');
      expect(component.icon).toStrictEqual(['fab', 'instagram']);
    });
    test('should set artstation icon', () => {
      component.webLink = {value: 'toto', type: 'ART_STATION'}

      expect(component.href).toEqual('https://www.artstation.com/toto');
      expect(component.icon).toStrictEqual(['fab', 'artstation']);
    });
    test('should set deviantart icon', () => {
      component.webLink = {value: 'toto', type: 'DEVIANT_ART'}

      expect(component.href).toEqual('https://www.deviantart.com/toto');
      expect(component.icon).toStrictEqual(['fab', 'deviantart']);
    });
    test('should set link icon as default', () => {
      component.webLink = {value: 'toto', type: null}

      expect(component.href).toEqual('toto');
      expect(component.icon).toStrictEqual(['fas', 'link']);
    });
    test('should set link icon', () => {
      component.webLink = {value: 'toto', type: 'OTHER'}

      expect(component.href).toEqual('toto');
      expect(component.icon).toStrictEqual(['fas', 'link']);
    });
  });
});
