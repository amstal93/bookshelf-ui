import {BehaviorSubject} from 'rxjs';
import {FormGroup} from '@angular/forms';

export interface UiData<T> {
  form: FormGroup;
  item: T;
  isSaved: BehaviorSubject<boolean>;
  start: BehaviorSubject<boolean>;
}

