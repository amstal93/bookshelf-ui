import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Image} from '../models/image';

@Component({
  selector: 'app-loader-img',
  template: `
      <mat-spinner *ngIf="loading"></mat-spinner>
      <img [class]="imgContainerClass"
           [hidden]="loading"
           (error)="onError()"
           (load)="onLoad()"
           [src]="src"
           [alt]="alt"/>
  `,
  styles: [`
    img.cover {
      width: 75%;
      height: auto;
    }
  `]
})
export class LoaderImgComponent implements OnInit, OnChanges {

  @Input() img: Image | null = null;
  @Input() imgContainerClass: string | null = null;

  private defaultImg = {
    src: 'assets/images/no_cover.svg',
    alt: 'No cover found'
  };

  public src: string;
  public alt: string;

  public loading = true;

  onLoad() {
    this.loading = false;
  }

  onError() {
    this.loading = false;
    this.loadImg(this.defaultImg);
  }

  ngOnInit() {
    this.loadImg(this.img !== null ? this.img : this.defaultImg);
  }

  loadImg(img: Image | null) {
    this.alt = img.alt;
    this.src = img.src;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loadImg(this.img !== null ? this.img : this.defaultImg);
  }
}
