import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-status-reading-icon',
  template: `
    mock icon reading
  `
})
export class MockBookStatusReadingIconComponent {
  @Input() withText: any;
}
