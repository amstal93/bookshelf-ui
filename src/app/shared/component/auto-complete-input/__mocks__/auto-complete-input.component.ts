import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-auto-complete-input',
  template: ` mock auto complete `
})
export class MockAutoCompleteInputComponent {
  @Input() form: any;
  @Input() placeholder: any;
  @Input() displayLabel: any;
  @Input() appearance: any;
  @Input() classes: any;
}
