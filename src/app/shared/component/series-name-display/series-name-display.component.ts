import {Component, Input} from '@angular/core';
import {Series} from "../../../core/model/series";

@Component({
  selector: 'app-series-name-display',
  template: `
    <div class="series-name-wrapper">
      <span class="mat-small" *ngIf="series.oneShot; else notOneShot">
        {{'SERIES.ONE_SHOT' | translate}}
      </span>

      <ng-template #notOneShot>
        <span style="opacity: 0.5" class="mat-small" *ngIf="!series.oneShot">
          {{ series.displayName }}
        </span>
      </ng-template>
    </div>
  `,
  styles: [`
    .series-name-wrapper {
      opacity: 0.5
    }
  `]
})
export class SeriesNameDisplayComponent {

  @Input() series: Series;

}
