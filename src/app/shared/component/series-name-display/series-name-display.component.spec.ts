import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SeriesNameDisplayComponent} from './series-name-display.component';
import {SeriesImpl} from "../../../core/model/impl/series-impl";

describe('SeriesNameDisplayComponent', () => {
  let component: SeriesNameDisplayComponent;
  let fixture: ComponentFixture<SeriesNameDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeriesNameDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesNameDisplayComponent);
    component = fixture.componentInstance;
    component.series = new SeriesImpl()
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
