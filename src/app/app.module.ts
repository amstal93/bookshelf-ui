import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {AdministrationModule} from './administration/administration.module';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {ConfigService} from './core/services/config.service';
import {filter} from 'rxjs/operators';
import {BookModule} from './book/book.module';
import {EventTypes, PublicEventsService} from 'angular-auth-oidc-client';
import {ArtistModule} from "./artist/artist.module";
import {AppRoutingModule} from "./app-routing.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {SharedModule} from './shared/shared.module';
import {AuthConfigModule} from './auth/auth-config.module';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {DateAdapter, MAT_DATE_LOCALE} from "@angular/material/core";
import {MomentDateAdapter} from '@angular/material-moment-adapter';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AuthConfigModule,
    TranslateModule.forRoot({
      defaultLanguage: 'fr',
      useDefaultLang: true,
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      isolate: false
    }),

    SharedModule,
    CoreModule,
    AdministrationModule,
    AppRoutingModule,
    BookModule,
    ArtistModule,
    ScrollingModule
  ],
  providers: [
    HttpClient,
    CookieService,
    ConfigService,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private readonly eventService: PublicEventsService) {
    this.eventService
      .registerForEvents()
      .pipe(filter((notification) => notification.type === EventTypes.ConfigLoaded))
      .subscribe();
  }
}
