import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';
import {BookType} from '../../../../../core/model/book-type';

@Component({
  selector: 'app-book-type-gestion-list',
  templateUrl: './book-type-gestion-list.component.html'
})
export class BookTypeGestionListComponent {

  bookTypeList$: Observable<BookType[]> = this.bookTypeAdministrationService.list$;

  constructor(
    private bookTypeAdministrationService: BookTypeAdministrationService
  ) {
  }
}
