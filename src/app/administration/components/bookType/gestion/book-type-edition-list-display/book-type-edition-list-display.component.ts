import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';
import {BookType} from '../../../../../core/model/book-type';
import {HasTimedProgressBar} from '../../../../../shared/has-timed-progress-bar';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-book-type-edition-list-display',
  templateUrl: './book-type-edition-list-display.component.html'
})
export class BookTypeEditionListDisplayComponent extends HasTimedProgressBar implements OnInit {

  @Input()
  bookType: BookType = {nbSeries: 0, id: 0, name: ''};
  form: FormGroup = this.fb.group({
    name: this.fb.control(''),
  });

  constructor(
    private fb: FormBuilder,
    private adminService: BookTypeAdministrationService
  ) {
    super();
  }

  ngOnInit() {
    this.init();
  }

  initForm(): void {
    this.form.setValue({
      name: this.bookType.name
    });
  }

  submit() {
    this.progressBarState = {display: true, type: 'indeterminate'};
    const bookType = this.form.value;
    bookType.id = this.bookType.id;
    this.adminService.update(bookType).subscribe(value => {
      this.bookType = value;
      this.adminService.getAll().subscribe();
      this.updateIsSaved();
      this.hideProgressBar();
    });
  }

  delete() {
    this.adminService.delete(this.bookType).pipe(switchMap(() => this.adminService.getAll())).subscribe();
  }

  // Workaround to fix a bug on nbBook set to 0 on input
  getNbBooks(): string {
    return (this.bookType.nbSeries || 0).toString();
  }

  isRemovable(): boolean {
    return this.bookType.nbSeries === 0;
  }
}
