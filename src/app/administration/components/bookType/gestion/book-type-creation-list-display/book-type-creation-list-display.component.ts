import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BookTypeAdministrationService} from '../../../../services/book-type-administration.service';

@Component({
  selector: 'app-book-type-creation-list-display',
  templateUrl: './book-type-creation-list-display.component.html'
})
export class BookTypeCreationListDisplayComponent {

  form: FormGroup = this.fb.group({
    name: this.fb.control(''),
  });

  constructor(
    private fb: FormBuilder,
    private bookTypeAdministrationService: BookTypeAdministrationService
  ) {
  }

  submit() {
    this.bookTypeAdministrationService.create(this.form.value).subscribe();
    this.form.reset();
  }
}
