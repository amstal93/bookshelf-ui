import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SeriesEditionListComponent} from './series-edition-list.component';
import {seriesAdministrationServiceMock} from '../../../../services/__mocks__/series-administration.service';
import {SeriesAdministrationService} from '../../../../services/series-administration.service';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MockProgressbarComponent} from '../../../../shared/progressbar/__mocks__/progressbar.component';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {MatFormFieldModule} from '@angular/material/form-field';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {BehaviorSubject} from 'rxjs';
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../../../shared/services/__mocks__/pagination.service";
import {RouterTestingModule} from "@angular/router/testing";
import {MockEditorFormComponent} from "../../../book/shared/editor-form/__mocks__/editor-form.component";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MockBookTypeFormComponent} from "../../../book/shared/book-type-form/__mocks__/book-type-form.component";

describe('SeriesEditionListComponent', () => {
  let component: SeriesEditionListComponent;
  let fixture: ComponentFixture<SeriesEditionListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SeriesEditionListComponent, MockProgressbarComponent, MockBookTypeFormComponent, MockEditorFormComponent],
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        NoopAnimationsModule,
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatTableModule,
        MatSlideToggleModule
      ],
      providers: [
        {provide: PaginationService, useValue: paginationServiceMock},
        {provide: SeriesAdministrationService, useValue: seriesAdministrationServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesEditionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('OnDestroy', () => {
      test('should unsub all subscription', waitForAsync(() => {
        jest.clearAllMocks();
        const obs: BehaviorSubject<number> = new BehaviorSubject<number>(0);
        let value = 0;
        component[`subscriptions`].push(obs.subscribe(v => value = v))
        obs.next(30);
        expect(value).toStrictEqual(30)
        component[`ngOnDestroy`]();
        obs.next(40);
        expect(value).toStrictEqual(30)
      }));
    });
    describe('isRemovable', () => {
      test('should return true', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        expect(component.isRemovable(0)).toBeTruthy()
      });
      test('should return false', () => {
        jest.clearAllMocks();
        component.uiDatas = [];
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            displayName: 'series',
            seriesBookCount: 30
          }
        })
        expect(component.isRemovable(0)).toBeFalsy()
      });
    });
    describe('switchToOneShot', () => {
      test('should disable displayName control', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: new FormGroup({displayName: new FormControl()}), isSaved: undefined, start: undefined,
          item: {
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        component.switchToOneShot({checked: true, source: null}, 0)
        expect(component.uiDatas[0].form.controls.displayName.disabled).toBeTruthy()
        expect(component.uiDatas[0].item.oneShot).toBeTruthy()
      });
      test('should enable displayName control', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: new FormGroup({displayName: new FormControl()}), isSaved: undefined, start: undefined,
          item: {
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        component.switchToOneShot({checked: false, source: null}, 0)
        expect(component.uiDatas[0].form.controls.displayName.enabled).toBeTruthy()
        expect(component.uiDatas[0].item.oneShot).toBeFalsy()
      });
    });
    describe('onFinish', () => {
      test('should call send form the extended class', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn<any, any>(component, `send`).mockImplementation()
        component.uiDatas.push({
          form: new FormGroup({
            displayName: new FormControl(),
            editor: new FormGroup({name: new FormControl('editor')})
          }),
          isSaved: undefined, start: undefined,
          item: {
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        component.onFinish(0);
        expect(spy).toHaveBeenNthCalledWith(1, component.uiDatas[0], {
          displayName: null,
          editor: 'editor',
          seriesBookCount: 0
        })
      });
      test('should enable displayName control', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: new FormGroup({displayName: new FormControl()}), isSaved: undefined, start: undefined,
          item: {
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        component.switchToOneShot({checked: false, source: null}, 0)
        expect(component.uiDatas[0].form.controls.displayName.enabled).toBeTruthy()
        expect(component.uiDatas[0].item.oneShot).toBeFalsy()
      });
    });
    describe('initForm', () => {
      test('should return an initiate form is not one shot', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            bookType: {name: null, id: null},
            editor: 'editor',
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).value)
          .toStrictEqual({
            bookType: {name: null, id: null},
            editor: {
              id: null,
              name: 'editor'
            },
            displayName: 'series'
          })
      });
      test('should return an initiate form is one shot', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            bookType: {name: 'bookType', id: 89},
            editor: 'editor',
            displayName: 'series',
            oneShot: true,
            seriesBookCount: 0
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).getRawValue())
          .toStrictEqual({
            bookType: {name: 'bookType', id: 89},
            editor: {
              id: null,
              name: 'editor'
            },
            displayName: 'series'
          })
      });
      test('should return an initiate form with bookType undifined', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            bookType: undefined,
            editor: 'editor',
            displayName: 'series',
            oneShot: true,
            seriesBookCount: 0
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).getRawValue())
          .toStrictEqual({
            bookType: {name: null, id: null},
            editor: {
              id: null,
              name: 'editor'
            },
            displayName: 'series'
          })
      });
      test('should return an initiate form with bookType name and id null', () => {
        jest.clearAllMocks();
        component.uiDatas.push({
          form: undefined, isSaved: undefined, start: undefined,
          item: {
            bookType: {name: null, id: null},
            editor: 'editor',
            displayName: 'series',
            oneShot: true,
            seriesBookCount: 0
          }
        })
        expect(component[`initForm`](component.uiDatas[0].item).getRawValue())
          .toStrictEqual({
            bookType: {name: null, id: null},
            editor: {
              id: null,
              name: 'editor'
            },
            displayName: 'series'
          })
      });
    });
    describe('getBookTypeControl', () => {
      test('should return the bookType FromControl', () => {
        component.uiDatas.push({
          form: new FormGroup({bookType: new FormGroup({name: new FormControl('best bookType'), id: new FormControl(89)})}),
          isSaved: undefined, start: undefined,
          item: {
            bookType: {name: 'best bookType', id: 89},
            editor: 'editor',
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        expect(component.getBookTypeFormGroup(0).value)
          .toStrictEqual({name: 'best bookType', id: 89})
      });
    });
    describe('getEditorGroup', () => {
      test('should return the editor FromControl', () => {
        component.uiDatas.push({
          form: new FormGroup({editor: new FormGroup({
              name: new FormControl('editorName')
            })}),
          isSaved: undefined, start: undefined,
          item: {
            bookType: {name: 'bookType', id: 89},
            editor: 'editor',
            displayName: 'series',
            seriesBookCount: 0
          }
        })
        expect(component.getEditorGroup(0).value)
          .toStrictEqual({name: 'editorName'})
      });
    });
    test('should return the searchCriteriaList', () => {
      expect(component[`getSearchCriteriaList`]('toto'))
        .toStrictEqual([
          {name: 'displayName', operation: ':', value: `*toto*`}
        ]);
    });
  });
});
