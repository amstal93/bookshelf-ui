import {Component, OnDestroy, OnInit} from '@angular/core';
import {SeriesAdministrationService} from '../../../../services/series-administration.service';
import {Series} from '../../../../../core/model/series';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {EditionList} from '../../../../shared/edition-list';
import {MatPaginatorIntl} from "@angular/material/paginator";
import {MyCustomPaginatorIntlService} from "../../../../../shared/services/my-custom-paginator-intl.service";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {ActivatedRoute} from "@angular/router";
import {SearchCriteria} from "../../../../../shared/services/entity.service";

type T = Series;

@Component({
  selector: 'app-series-edition-list',
  templateUrl: './series-edition-list.component.html',
  styleUrls: ['../../../../administration-edition.scss'],
  styles: [`
    .mat-column-displayName {
      flex: none;
      width: 50%;
    }

    .mat-column-editor {
      flex: none;
      width: 25%;
    }

    .mat-column-action {
      flex: none;
      width: 5%;
    }
  `],
  providers: [{
    provide: MatPaginatorIntl,
    useFactory: (translate: TranslateService) => new MyCustomPaginatorIntlService('SERIES', translate),
    deps: [TranslateService]
  }]
})
export class SeriesEditionListComponent extends EditionList<T> implements OnInit, OnDestroy {
  public displayedColumns: string[] = ['editor', 'bookType', 'displayName', 'oneShot', 'action'];

  constructor(
    adminService: SeriesAdministrationService,
    paginationService: PaginationService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    route: ActivatedRoute
  ) {
    super(adminService, paginationService, route);
  }

  ngOnInit(): void {
    super.onInit()
  }

  ngOnDestroy(): void {
    super.onDestroy()
  }

  isRemovable(index: number): boolean {
    return +this.uiDatas[index].item.seriesBookCount === 0;
  }

  onFinish(index: number): void {
    const uiData = this.uiDatas[index]
    const series = {...uiData.item, ...uiData.form.value}
    series.editor = uiData.form.value.editor.name;
    this.send(uiData, series)
  }

  protected initForm(item: T): FormGroup {
    const form = this.fb.group({
      displayName: this.fb.control(item.displayName),
      editor: this.fb.group({
        id:  null,
        name:  item.editor,
      }),
      bookType: this.fb.group({
        id: item.bookType?.id ?? null,
        name: item.bookType?.name ?? null
      }),
    });
    if (item.oneShot) {
      form.controls['displayName'].disable()
    }
    return form;
  }

  switchToOneShot(event: MatSlideToggleChange, index: number) {
    this.uiDatas[index].item.oneShot = event.checked
    if (this.uiDatas[index].item.oneShot) {
      this.uiDatas[index].form.controls['displayName'].disable()
    } else {
      this.uiDatas[index].form.controls['displayName'].enable()
    }
  }

  getBookTypeFormGroup(index: number): FormGroup {
    return this.uiDatas[index].form.controls.bookType as FormGroup
  }

  getEditorGroup(index: number): FormGroup {
    return this.uiDatas[index].form.controls.editor as FormGroup
  }

  protected getSearchCriteriaList(search: string): SearchCriteria[] {
    return [{name: 'displayName', operation: ':', value: `*${search}*`}]
  }
}
