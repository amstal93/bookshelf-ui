import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookAddComponent} from './book-add.component';
import {BookAdministrationService} from '../../../../services/book-administration.service';
import {bookAdministrationServiceMock} from '../../../../services/__mocks__/book-administration.service';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {RouterTestingModule} from '@angular/router/testing';
import {MockBookFormComponent} from '../../shared/book-form/__mocks__/book-form.component';

describe('BookAddComponent', () => {
  let component: BookAddComponent;
  let fixture: ComponentFixture<BookAddComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookAddComponent,
        MockBookFormComponent
      ],
      imports: [
        NgxTranslateTestingModule,
        RouterTestingModule
      ],
      providers: [
        {provide: BookAdministrationService, useValue: bookAdministrationServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});
