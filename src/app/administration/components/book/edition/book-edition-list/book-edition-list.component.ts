import {Component, OnDestroy, OnInit} from '@angular/core';
import {Book, BookStatus} from '../../../../../core/model/book';
import {BookAdministrationService} from '../../../../services/book-administration.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EditionList} from '../../../../shared/edition-list';
import {MatPaginatorIntl} from "@angular/material/paginator";
import {TranslateService} from "@ngx-translate/core";
import {MyCustomPaginatorIntlService} from "../../../../../shared/services/my-custom-paginator-intl.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {UiData} from "../../../../../shared/ui-data";
import {SearchCriteria} from "../../../../../shared/services/entity.service";


type T = Book;

@Component({
  selector: 'app-book-edition-list',
  templateUrl: './book-edition-list.component.html',
  styleUrls: ['../../../../administration-edition.scss'],
  styles: [`
    .mat-column-seriesEditor {
      flex: none;
      width: 35%;
    }

    .mat-column-title {
      flex: none;
      width: 45%;
    }

    .mat-column-tome {
      flex: none;
      width: 5%;
    }

    .mat-column-action {
      flex: none;
      width: 15%;
    }

  `],
  providers: [{
    provide: MatPaginatorIntl,
    useFactory: (translate: TranslateService) => new MyCustomPaginatorIntlService('BOOKS', translate),
    deps: [TranslateService]
  }]
})
export class BookEditionListComponent extends EditionList<T> implements OnInit, OnDestroy {
  public displayedColumns: string[] = ['seriesEditor', 'title', 'tome', 'status', 'action'];

  get statusValues(): BookStatus[] {
    return Book.BookStatus.values()
  }

  constructor(
    adminService: BookAdministrationService,
    paginationService: PaginationService,
    private fb: FormBuilder,
    private router: Router,
    route: ActivatedRoute
  ) {
    super(adminService, paginationService, route);
  }

  ngOnInit(): void {
    super.onInit()
  }

  ngOnDestroy(): void {
    super.onDestroy()
  }

  protected initForm(item: T): FormGroup {
    const form = this.fb.group({
      title: item.title,
      tome: this.fb.control(item.tome, {}),
      status: item.status
    });
    if (item.series.oneShot) {
      form.controls['tome'].disable()
    }
    return form
  }

  public goToEditPage(index: number): void {
    this.router.navigate(['administration', 'book', 'edit', this.uiDatas[index].item.isbn]).then()
  }

  protected send(uiData: UiData<T>, t: T) {
    delete t.series;
    delete t.editor;
    this.subscriptions.push(
      this.adminService.patch(t, false)
        .subscribe((value) => {
          uiData.item = value;
          uiData.start.next(false);
          uiData.isSaved.next(true);
          setTimeout(() => uiData.isSaved.next(false), 3000);
        })
    );
  }

  protected getSearchCriteriaList(search: string): SearchCriteria[] {
    return [
      {name: 'title', operation: ':', value: `*${search}*`},
      {name: 'editor', operation: ':', value: `*${search}*`, or: true},
      {name: 'series', operation: ':', value: `*${search}*`, or: true}
    ]
  }

}
