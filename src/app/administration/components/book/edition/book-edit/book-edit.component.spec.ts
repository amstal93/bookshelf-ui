import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookEditComponent} from './book-edit.component';
import {MockBookFormComponent} from '../../shared/book-form/__mocks__/book-form.component';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterTestingModule} from '@angular/router/testing';

describe('BookEditComponent', () => {
  let component: BookEditComponent;
  let fixture: ComponentFixture<BookEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BookEditComponent, MockBookFormComponent],
      imports: [
        NgxTranslateTestingModule,
        RouterTestingModule,
        FlexLayoutModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});
