import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {debounceTime} from "rxjs/operators";
import {BookFormService} from "../../../../services/book-form.service";

@Component({
  selector: 'app-book-metadata-form',
  template: `
    <form fxFlex="100" [formGroup]="form" fxLayout="column">
      <div fxFlex="100" fxLayout="column">
        <h4>{{'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.TITLE' | translate}}</h4>
        <div fxLayout="row" fxLayoutGap="1rem">
          <app-date-input-acquisition [form]="getAcquisitionDateFrom()"></app-date-input-acquisition>
          <mat-slide-toggle fxFlex #toggle (change)="toggleOfferedStatus($event.checked)"
                            labelPosition="after" fxFlexAlign="end center">
            {{'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.GIFT' | translate}}
          </mat-slide-toggle>
        </div>
      </div>
      <div fxLayout="row" fxLayoutGap="1rem" *ngIf="!toggle.checked">
        <mat-form-field fxFlex>
          <label>
            <input formControlName="price"
                   matInput min="0"
                   type="number" step="0.1"
                   [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.PURCHASE_PRICE' | translate"/>
          </label>
        </mat-form-field>
        <mat-form-field fxFlex>
          <mat-select formControlName="priceCurrency"
                      [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.ACQUISITION.PURCHASE_CURRENCY' | translate">
            <mat-option [value]="'EUR'">EUR</mat-option>
            <mat-option [value]="'USD'">USD</mat-option>
          </mat-select>
        </mat-form-field>
      </div>
      <div fxFlex="100" fxLayout="column">
        <h4>{{'BOOK.FORM.METADATA.SUBSECTION.EDITION.TITLE' | translate}}</h4>
        <div fxLayout="row" fxLayoutGap="1rem">
          <mat-form-field fxFlex>
            <label>
              <input matInput type="number" min="0"
                     formControlName="pageCount"
                     [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.EDITION.PAGE_COUNT' | translate"/>
            </label>
          </mat-form-field>
          <app-date-input-original-release [form]="getOriginalReleaseDateFrom()"></app-date-input-original-release>
        </div>
      </div>
    </form>
  `
})
export class BookMetadataFormComponent implements OnInit {

  @Input() form: FormGroup;
  @Output() public updateOfferedStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

  toggleOfferedStatus(status: boolean): void {
    this.updateOfferedStatus.emit(status)
  }

  constructor(
    private bookFormService: BookFormService
  ) {
  }

  ngOnInit(): void {
    this.form.get('price').valueChanges
      .pipe(debounceTime(1000))
      .subscribe(v => this.bookFormService.setCurrencyMandatory(v))
  }


  getAcquisitionDateFrom(): FormControl {
    return this.form.get('acquisitionDate') as FormControl;
  }

  getOriginalReleaseDateFrom(): FormControl {
    return this.form.get('originalReleaseDate') as FormControl;
  }
}
