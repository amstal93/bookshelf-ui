import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookMetadataFormComponent} from './book-metadata-form.component';
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {
  NgxTranslateTestingModule
} from "../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {BookFormService} from "../../../../services/book-form.service";
import {bookFormServiceMock} from "../../../../services/__mocks__/book-form.service";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('BookMetadataFormComponent', () => {
  let component: BookMetadataFormComponent;
  let fixture: ComponentFixture<BookMetadataFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookMetadataFormComponent],
      imports: [
        NoopAnimationsModule,
        NgxTranslateTestingModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatInputModule,
        MatSlideToggleModule
      ],
      providers: [
        {provide: BookFormService, useValue: bookFormServiceMock},
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookMetadataFormComponent);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      acquisitionDate: new FormControl(null),
      offered: new FormControl(false),
      originalReleaseDate: new FormControl(null),
      pageCount: new FormControl(null),
      price: new FormControl(null),
      priceCurrency: new FormControl(null)
    });
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('submit', () => {
      test('should emit and reset the form', waitForAsync(() => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component.updateOfferedStatus, `emit`).mockImplementation(() => ({}));
        component.toggleOfferedStatus(true)
        expect(spy).toHaveBeenNthCalledWith(1, true)
      }));
    });
  });
});
