import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DateInputAcquisitionComponent} from './date-input-acquisition.component';
import {
  NgxTranslateTestingModule
} from "../../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";

describe('DateInputAquisitionComponent', () => {
  let component: DateInputAcquisitionComponent;
  let fixture: ComponentFixture<DateInputAcquisitionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateInputAcquisitionComponent ],
      imports: [
        NgxTranslateTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateInputAcquisitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
