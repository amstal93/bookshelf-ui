import {ComponentFixture, TestBed} from '@angular/core/testing';
import {DateInputOriginalReleaseComponent} from './date-input-original-release.component';
import {
  NgxTranslateTestingModule
} from "../../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import moment from "moment";
import {MatDatepicker, MatDatepickerModule} from "@angular/material/datepicker";
import {FormControl, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {MatMomentDateModule} from "@angular/material-moment-adapter";

describe('DateInputOriginalReleaseComponent', () => {
  let component: DateInputOriginalReleaseComponent;
  let fixture: ComponentFixture<DateInputOriginalReleaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DateInputOriginalReleaseComponent],
      imports: [
        NgxTranslateTestingModule,
        NoopAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatMomentDateModule,
        ReactiveFormsModule
      ],
      providers: [MatMomentDateModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateInputOriginalReleaseComponent);
    component = fixture.componentInstance;
    component.form = new FormControl(null)
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('should set the date init date null', async () => {
      component.setMonthAndYear(moment("2025-05"), {close() {}} as MatDatepicker<any>);
      expect(component.form.value.format("yyyy-MM")).toStrictEqual("2025-05")
    });
    test('should set the date init date not null', async () => {
      component.form.setValue(moment("2025-08-01T20:00:00.000Z"))
      component.setMonthAndYear(moment("2025-05"), {close() {}} as MatDatepicker<any>);
      expect(component.form.value.format("yyyy-MM")).toStrictEqual("2025-05")
    });
  });
});
