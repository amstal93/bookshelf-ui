import {Component, Input} from '@angular/core';
import {FormControl} from "@angular/forms";
import {MAT_DATE_FORMATS} from "@angular/material/core";
import moment, {Moment} from "moment";
import {MatDatepicker} from "@angular/material/datepicker";

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-date-input-original-release',
  template: `
    <mat-form-field fxFlex>
      <label>
        <input [matDatepicker]="dp" matInput
               [formControl]="form"
               [placeholder]="'BOOK.FORM.METADATA.SUBSECTION.EDITION.ORIGINAL_RELEASE_DATE' | translate"/>
      </label>
      <mat-hint>MM/YYYY</mat-hint>
      <mat-datepicker-toggle matSuffix [for]="dp"></mat-datepicker-toggle>
      <mat-datepicker #dp
                      startView="multi-year"
                      (monthSelected)="setMonthAndYear($event, dp)"
                      panelClass="example-month-picker">
      </mat-datepicker>
    </mat-form-field>
    `,
  providers: [{provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}]
})
export class DateInputOriginalReleaseComponent {
  @Input()
  form: FormControl

  setMonthAndYear(normalizedMonthAndYear: Moment, datepicker: MatDatepicker<Moment>) {
    let ctrlValue = this.form.value;
    if (ctrlValue === null) {
      ctrlValue = moment().day(1).hours(0).minutes(0).seconds(0).millisecond(0);
    }
    ctrlValue.year(normalizedMonthAndYear.year());
    ctrlValue.month(normalizedMonthAndYear.month());
    this.form.setValue(ctrlValue);
    datepicker.close();
  }
}
