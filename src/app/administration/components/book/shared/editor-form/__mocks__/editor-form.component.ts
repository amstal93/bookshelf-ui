import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-editor-form',
  template: '<div>Editor Form Mock</div>'
})
export class MockEditorFormComponent {

  @Input() form: any;
  @Input() appearance: any;
  @Input() classes: any;

}
