import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {BookFormService} from '../../../../services/book-form.service';
import {ArtistFormComponent} from '../artist-form/artist-form.component';
import {map, startWith, switchMap} from 'rxjs/operators';
import {RoleAdministrationService} from '../../../../services/role-administration.service';
import {Observable, Subscription} from 'rxjs';
import {Role} from '../../../../../core/model/role';
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-contract-form',
  templateUrl: './contract-form.component.html',
  providers: [RoleAdministrationService]
})
export class ContractFormComponent implements OnInit, OnDestroy {
  @Input() form: FormGroup;
  @Input() index: number;
  @Output() delete: EventEmitter<number> = new EventEmitter();

  @ViewChildren(ArtistFormComponent) artistFormComponents: QueryList<ArtistFormComponent>;

  roleFilteredOptions$: Observable<Role[]>;
  private subscriptions: Subscription[] = [];

  editRoleName = false;

  constructor(
    private bookFormService: BookFormService,
    private rolesService: RoleAdministrationService
  ) {
  }

  // TODO use AutoCompleteInputComponent ??
  ngOnInit(): void {
    this.roleFilteredOptions$ = this.rolesService.list$;
    this.subscriptions.push(
      this.form.get('role.name').valueChanges
        .pipe(
          startWith(''),
          switchMap((value: string) =>
            this.rolesService.searchAutocomplete(
              [{name: 'name', operation: ':', value: `*${value}*`}],
              [{key: 'sort', value: 'count'}])
          ),
            map((response: HttpResponse<Role[]>) => response.body)
        )
        .subscribe()
    );
  }

  removeContract() {
    this.delete.emit(this.index);
  }

  addArtist() {
    this.bookFormService.addArtist(this.index, {name: '', webLinks: [], id: null});
  }

  deleteArtist(artistIndex: number) {
    this.bookFormService.deleteArtist(this.index, artistIndex);
  }

  getArtistsFormGroup(): FormGroup[] {
    return (this.form.get('artists') as FormArray).controls as FormGroup[];
  }

  updateRole(role: Role = null): void {
    const roleControl = this.form.get('role');
    roleControl.setValue(role === null ? {name: roleControl.value.name.trim(), id: null} : role);
    this.editRoleName = false;
  }

  currentRoleName(): string {
    return this.form.get('role').value.name;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  nonEmptyArtists() {
    return this.form.value.artists.filter(a => a.name !== '' && a.name !== null).length
  }

  currentSeriesName() {
    return this.bookFormService.getCurrentSeries().name;
  }
}
