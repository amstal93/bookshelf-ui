import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-contract-form',
  template: '<div>Contract Form Mock</div>'
})
export class MockContractFormComponent {

  @Input() form: any;
  @Input() index: any;
  @Output() delete: any;
}
