import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-form',
  template: '<div>Book Form Mock</div>'
})
export class MockBookFormComponent {

  @Input()
  data$: any;

  @Input()
  edition: any;
}
