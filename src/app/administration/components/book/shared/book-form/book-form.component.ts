import {Component, OnDestroy, OnInit} from '@angular/core';
import {DisplayImage} from '../../../../../shared/display-image';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {BookFormService} from '../../../../services/book-form.service';
import {Book} from '../../../../../core/model/book';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {TranslateService} from '@ngx-translate/core';
import {BookAdministrationService} from '../../../../services/book-administration.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {Editor} from '../../../../../core/model/editor';
import {Series} from '../../../../../core/model/series';
import {NgxDropzoneChangeEvent} from 'ngx-dropzone';
import {Image} from '../../../../../shared/models/image';
import {Role} from '../../../../../core/model/role';
import {MatDialog} from "@angular/material/dialog";
import {TaskService} from "../../../../services/task.service";
import {DialogConfirmComponent} from "../../../../../shared/confirm-modal/dialog-confirm.component";
import {BookImpl} from "../../../../../core/model/impl/book-impl";
import {filter} from "rxjs/operators";
import {NotificationService} from "../../../../../shared/services/notification.service";
import {Moment} from "moment";

export class BookFormData {
  edition: boolean = false;
  data: Book = new BookImpl();
}

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html'
})
export class BookFormComponent extends DisplayImage implements OnInit, OnDestroy {
  uploadCover = false;
  private cover: BehaviorSubject<Image> = new BehaviorSubject<Image>(null);

  get cover$(): Observable<Image> {
    return this.cover.asObservable();
  }

  constructor(
    private bookAdministrationService: BookAdministrationService,
    private bookFormService: BookFormService,
    private translateService: TranslateService,
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private taskService: TaskService
  ) {
    super('/files/search/covers');
  }

  public form: FormGroup;
  private subscriptions: Subscription[] = [];
  public contracts: FormArray;
  public editor: FormGroup;
  public series: FormGroup;
  public metadata: FormGroup;

  public book: Book = null;
  public edition = false;
  public translateCategory = 'ADD'

  ngOnInit(): void {
    this.subscriptions.push(
      this.bookFormService.bookForm$
        .subscribe(bookForm => {
          this.form = bookForm;
          this.contracts = this.form.get('contracts') as FormArray;
          this.editor = this.form.get('editor') as FormGroup;
          this.series = this.form.get('series') as FormGroup;
          this.metadata = this.form.get('metadata') as FormGroup;
        }),
      this.route.data
        .subscribe((data: BookFormData) => {
          this.edition = data.edition
          if (this.edition) {
            this.imagePath = '/files/covers';
            this.translateCategory = 'EDIT';
          } else {
            this.imagePath = '/files/search/covers';
            this.translateCategory = 'ADD';
          }
          this.book = data.data;
          this.bookFormService.initForm(this.book);
          this.cover.next(this.getImg(this.book.cover));
        }),

      (this.form.get('title') as FormControl).valueChanges
        .pipe(filter(() => this.book.series.oneShot))
        .subscribe(value => {
          this.form.get('series').patchValue({
            name: value,
            displayName: value
          }, { emitEvent: false });
        })
    );
  }

  addContract(role: Role) {
    this.bookFormService.addContract(role);
  }

  deleteContract(index: number) {
    this.bookFormService.deleteContract(index);
  }

  switchToOneShot(event: MatSlideToggleChange) {
    this.book.series.oneShot = event.checked;
    this.bookFormService.toggleOneShot(event.checked);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  submit(): void {
    const editor = this.buildEditor();
    const series = this.buildSeries(editor);
    const book = this.buildBook(editor, series);
    book.isbn = this.book.isbn;
    (this.edition ? this.bookAdministrationService.update(book) : this.bookAdministrationService.create(book))
      .subscribe(
        {
          next: (b) => this.success(b),
          error: (error) => this.error(error, book)
        }
      );
  }

  private buildEditor(): Editor {
    return {...this.book.editor, ...this.form.value.editor};
  }

  private buildSeries(editor: Editor): Series {
    return {
      ...this.book.series, ...this.form.getRawValue().series,
      id: this.book.series.name !== this.form.getRawValue().series.name ? null : (this.form.getRawValue().series.id || this.book.series.id),
      editor: editor.name,
      oneShot: this.book.series.oneShot,
    };
  }

  private buildBook(editor: Editor, series: Series): Book {
    this.book.cover = this.book.cover !== null && this.book.cover !== undefined ? this.book.cover.split('?')[0] : null;
    const book = {...this.book, ...this.form.getRawValue(), series, editor, authors: this.form.value.authors};
    if (book.metadata.acquisitionDate !== null) {
      book.metadata.acquisitionDate = (this.form.value.metadata.acquisitionDate as Moment).format('yyyy/MM/DD')
    }
    if (book.metadata.originalReleaseDate !== null) {
      book.metadata.originalReleaseDate = (this.form.value.metadata.originalReleaseDate as Moment).format('yyyy/MM')
    }
    return book
  }

  private success(b: Book) {
    // TODO move that to a service ?
    this.taskService.getTaskTodo().subscribe();
    if (this.edition) {
      this.notificationService.displayMessage(`BOOK.EDIT.SUCCESS`, {isbn: b.isbn});
      this.router.navigate(['/', 'administration', 'book', 'edition']);
    } else {
      return this.dialog.open(DialogConfirmComponent, {
        width: '450px',
        data: {
          title: this.translateService.instant('BOOK.ADD.SUCCESS.CONTINUE_MODAL.TITLE'),
          message: this.translateService.instant('BOOK.ADD.SUCCESS.CONTINUE_MODAL.MESSAGE', {isbn: b.isbn}),
          question: this.translateService.instant('BOOK.ADD.SUCCESS.CONTINUE_MODAL.QUESTION')
        }
      }).afterClosed().subscribe((res: boolean) => {
        if (!res) {
          this.router.navigate(['/', 'book', 'list'], {queryParams: {bookType: b.series.bookType}});
        } else {
          this.router.navigate(['/', 'administration', 'book', 'add'], {queryParams: {withMenu: true}});
        }
      });
    }
  }

  private error(err: HttpErrorResponse, book: Book) {
    if (err.status === 409) {
      this.notificationService.displayMessage(`BOOK.${this.translateCategory}.ERROR.ALREADY_EXIST`, {
        isbn: book.isbn,
        arkId: book.arkId
      });
    } else {
      this.notificationService.displayMessage(`BOOK.${this.translateCategory}.ERROR.GENERIC`, {isbn: book.isbn});
    }
  }

  resetImage() {
    this.imagePath = '/files/covers';
  }

  onSelect(event: NgxDropzoneChangeEvent): void {
    this.uploadCover = true;
    this.imagePath = '/files/search/covers';
    if (event.addedFiles[0]) {
      this.bookAdministrationService.uploadCover(event.addedFiles[0], this.form.value.isbn)
        .subscribe({
          next: ({cover}) => {
            this.cover.next(this.getImg(cover));
            this.book.cover = cover;
            this.uploadCover = false;
          },
          error: _ => this.uploadCover = false
        });
    }
  }

  getFormContractsFromGroups(): FormGroup[] {
    return this.contracts.controls as FormGroup[];
  }

  getBookTypeFormGroup(): FormGroup {
    return this.series.controls.bookType as FormGroup;
  }

  getSeries() {
    return {...this.book.series, ...this.form.getRawValue().series};
  }

  onSeriesSelection(series: Series) {
    this.bookFormService.updateSeries(series)
  }

  onEditorSelection(editor: Editor) {
    this.bookFormService.updateEditor(editor)
  }

  onUpdateOfferedStatus(status: boolean) {
    this.bookFormService.updateOfferedBookStatus(status)
  }
}
