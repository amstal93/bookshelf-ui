import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookFormComponent} from './book-form.component';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatChipsModule} from '@angular/material/chips';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MockLoaderImgComponent} from '../../../../../shared/loader-img/__mocks__/loader-img.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ReactiveFormsModule} from '@angular/forms';
import {MatListModule} from '@angular/material/list';
import {BookAdministrationService} from '../../../../services/book-administration.service';
import {bookAdministrationServiceMock} from '../../../../services/__mocks__/book-administration.service';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MockTitleDisplayComponent} from '../../../../../shared/title-display/__mocks__/title-display.component';
import {MockBookTypeFormComponent} from '../book-type-form/__mocks__/book-type-form.component';
import {MockSeriesFormComponent} from '../series-form/__mocks__/series-form.component';
import {MockEditorFormComponent} from '../editor-form/__mocks__/editor-form.component';
import {MatIconModule} from '@angular/material/icon';
import {MockArtistFormComponent} from '../artist-form/__mocks__/artist-form.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RouterTestingModule} from '@angular/router/testing';
import {BookImpl} from '../../../../../core/model/impl/book-impl';
import {BookFormService} from '../../../../services/book-form.service';
import {bookFormServiceMock} from '../../../../services/__mocks__/book-form.service';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {By} from '@angular/platform-browser';
import {BehaviorSubject, of, throwError} from 'rxjs';
import {MockRoleFormComponent} from '../role-form/__mocks__/role-form.component';
import {MockContractFormComponent} from '../contract-form/__mocks__/contract-form.component';
import {ActivatedRoute} from "@angular/router";
import {MatDialogModule} from "@angular/material/dialog";
import {TaskService} from "../../../../services/task.service";
import {taskServiceMock} from "../../../../services/__mocks__/task.service";
import {SeriesImpl} from "../../../../../core/model/impl/series-impl";
import {NotificationService} from "../../../../../shared/services/notification.service";
import {notificationServiceMock} from "../../../../../shared/services/__mocks__/notification.service";
import {MockBookMetadataFormComponent} from "../book-metadata-form/__mocks__/book-metadata-form.component";
import {MatTabsModule} from "@angular/material/tabs";

const data = new BehaviorSubject<any>({data: new BookImpl(), edition: false})

const routeMock = {
  data: data,
  snapshot: {
    data: data.value
  }
};

describe('BookFormComponent', () => {
  let component: BookFormComponent;
  let fixture: ComponentFixture<BookFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookFormComponent,
        MockLoaderImgComponent,
        MockTitleDisplayComponent,
        MockBookTypeFormComponent,
        MockSeriesFormComponent,
        MockEditorFormComponent,
        MockArtistFormComponent,
        MockRoleFormComponent,
        MockContractFormComponent,
        MockBookMetadataFormComponent
      ],
      imports: [
        FontAwesomeTestingModule,
        NoopAnimationsModule,
        NgxTranslateTestingModule,
        RouterTestingModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatListModule,
        MatDialogModule,
        MatIconModule,
        MatSnackBarModule,
        MatTabsModule
      ],
      providers: [
        {provide: ActivatedRoute, useValue: routeMock},
        {provide: BookAdministrationService, useValue: bookAdministrationServiceMock},
        {provide: BookFormService, useValue: bookFormServiceMock},
        {provide: NotificationService, useValue: notificationServiceMock},
        {provide: TaskService, useValue: taskServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(waitForAsync(() => {
    jest.clearAllMocks();
  }));

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
    describe('CREATION', () => {
      test('should create', () => {
        expect(component[`imgPath`]).toStrictEqual('/files/search/covers');
      });
    });
    describe('EDITION', () => {
      test('should create', () => {
        data.next({...data.value, edition: true});
        // component.ngOnInit();
        expect(component[`imgPath`]).toStrictEqual('/files/covers');
      });
    });
  });
  describe('Typescript test', () => {
    test('should call addAuthor', () => {
      component.addContract({name: 'role1'});
      expect(bookFormServiceMock.addContract).toHaveBeenCalledTimes(1);
    });
    test('should return the series default', () => {
      component.form.get('series').get('displayName').setValue('bonjour')
      expect(component.getSeries()).toStrictEqual({
        bookType: {
          id: null,
          name: null
        },
        displayName: 'bonjour',
        editor: null,
        id: null,
        name: "",
        oneShot: false,
        seriesBookCount: 0,
      });
    });
    test('should call updateOfferedStatus', () => {
      component.onUpdateOfferedStatus(true)
      expect(bookFormServiceMock.updateOfferedBookStatus).toHaveBeenNthCalledWith(1, true);
    });
    test('should call updateSeries', () => {
      component.onSeriesSelection({
        name: "test",
        id: 123456,
        displayName: "", seriesBookCount: 0
      })
      expect(bookFormServiceMock.updateSeries).toHaveBeenNthCalledWith(1, {
        name: "test",
        id: 123456,
        displayName: "", seriesBookCount: 0
      });
    });
    test('should call updateEditor', () => {
      component.onEditorSelection({
        name: "test",
        id: 123456
      })
      expect(bookFormServiceMock.updateEditor).toHaveBeenNthCalledWith(1, {
        name: "test",
        id: 123456
      });
    });
    test('should return the series with displayName disabled', () => {
      component.form.get('series').get('displayName').disable()
      component.form.get('series').get('displayName').setValue('bonjour')
      expect(component.getSeries()).toStrictEqual({
        bookType: {
          id: null,
          name: null
        },
        displayName: 'bonjour',
        editor: null,
        id: null,
        name: "",
        oneShot: false,
        seriesBookCount: 0,
      });
    });
    test('should call deleteAuthor', () => {
      component.deleteContract(1);
      expect(bookFormServiceMock.deleteContract).toHaveBeenNthCalledWith(1, 1);
    });
    test('should call switchToOneShot', () => {
      component.switchToOneShot({checked: false, source: undefined});
      expect(bookFormServiceMock.toggleOneShot).toHaveBeenNthCalledWith(1, false);
    });
    describe('GET TRANSLATE CATEGORY', () => {
      test('should call return ADD', () => {
        data.next({...data.value, edition: false});
        expect(component.translateCategory).toStrictEqual('ADD');
      });
      test('should call return EDIT', () => {
        data.next({...data.value, edition: true});
        expect(component.translateCategory).toStrictEqual('EDIT');
      });
    });
    describe('SUCCESS', () => {
      const b = new BookImpl();
      b.series = new SeriesImpl()
      b.series.bookType = {name: 'bookType', id: 98}
      b.isbn = 'isbn';
      test('should call return ADD then move', () => {
        data.next({...data.value, edition: false});
        const spyNavigate = jest.spyOn<any, any>(component[`router`], `navigate`).mockReturnValue(Promise.resolve(true));
        const spyDialog = jest.spyOn<any, any>(component[`dialog`], `open`).mockImplementation(() => ({afterClosed: () => of(false)}));
        component[`success`](b);
        expect(spyDialog).toHaveBeenCalledTimes(1);
        expect(spyNavigate).toHaveBeenNthCalledWith(1, ['/', 'book', 'list'], {
          queryParams: {
            bookType: {
              name: 'bookType',
              id: 98
            }
          }
        });
      });
      test('should call return ADD then stay', () => {
        data.next({...data.value, edition: false});
        const spyNavigate = jest.spyOn<any, any>(component[`router`], `navigate`).mockReturnValue(Promise.resolve(true));
        const spyDialog = jest.spyOn<any, any>(component[`dialog`], `open`).mockImplementation(() => ({afterClosed: () => of(true)}));
        component[`success`](b);
        expect(spyDialog).toHaveBeenCalledTimes(1);
        expect(taskServiceMock.getTaskTodo).toHaveBeenCalledTimes(1);
        expect(spyNavigate).toHaveBeenNthCalledWith(1, ['/', 'administration', 'book', 'add'], {queryParams: {withMenu: true}});
      });
      test('should call return EDIT', () => {
        data.next({...data.value, edition: true});
        const spyNavigate = jest.spyOn<any, any>(component[`router`], `navigate`).mockReturnValue(Promise.resolve(true));
        component[`success`](b);
        expect(notificationServiceMock.displayMessage).toHaveBeenNthCalledWith(1, `BOOK.EDIT.SUCCESS`, {isbn: 'isbn'});
        expect(spyNavigate).toHaveBeenNthCalledWith(1, ['/', 'administration', 'book', 'edition']);
      });
    });
    describe('SUBMIT', () => {
      const book = {
        editor: {name: 'editorName', id: 1},
        tome: '3',
        isbn: null,
        year: null,
        arkId: null,
        cover: null,
        collection: null,
        series: {
          name: 'seriesName',
          editor: 'editorName',
          displayName: 'seriesDisplayName',
          id: 0,
          "bookType": null,
          oneShot: true
        },
        contracts: []
      };
      const expectedBook = {
        ...book,
        title: null,
        authors: undefined,
        series: {...book.series, bookType: {name: 'BD', id: null}},
        editor: {...book.editor},
        metadata: {
          acquisitionDate: null,
          offered: false,
          originalReleaseDate: null,
          pageCount: null,
          price: null,
          priceCurrency: null,
        }
      };

      beforeEach(() => {
        component.book = {...book} as BookImpl;
        component.form.patchValue({
          tome: '3',
          series: {
            name: 'seriesName',
            displayName: 'seriesDisplayName',
            bookType: {name: 'BD', id: null}
          },
          editor: {
            id: 1,
            name: 'editorName'
          }
        });
      });
      describe('SUCCESS', () => {
        let spy;
        beforeEach(() => spy = jest.spyOn<any, any>(component, `success`).mockImplementation(() => ({})));

        test('should create book', waitForAsync(() => {
          component.edition = false;
          component.submit();
          expect(bookAdministrationServiceMock.create).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, {});
        }));
        test('should edit book', waitForAsync(() => {
          component.edition = true;
          component.book.cover = 'toto?tata';
          component.submit();
          expect(bookAdministrationServiceMock.update).toHaveBeenNthCalledWith(1, {...expectedBook, cover: 'toto'});
          expect(spy).toHaveBeenNthCalledWith(1, {...expectedBook, cover: 'toto'});
        }));
        test('should edit with id', waitForAsync(() => {
          component.form.patchValue({
            tome: '3',
            series: {
              name: 'new seriesName',
              displayName: 'seriesDisplayName',
              bookType: 'BD'
            },
            editor: {
              name: 'editorName'
            }
          });
          component.edition = true;
          component.book.cover = 'toto?tata';
          component.submit();
          expect(bookAdministrationServiceMock.update).toHaveBeenNthCalledWith(1,
            {
              ...expectedBook,
              cover: 'toto',
              series: {...expectedBook.series, id: null, name: "new seriesName"}
            }
          );
          expect(spy).toHaveBeenNthCalledWith(1, {
            ...expectedBook,
            cover: 'toto',
            series: {...expectedBook.series, id: null, name: "new seriesName"}
          });
        }));
      });
      describe('ERROR', () => {
        let spy;
        beforeEach(() => spy = jest.spyOn<any, any>(component, `error`).mockImplementation(() => ({})));
        test('should create book', waitForAsync(() => {
          component.edition = false;
          bookAdministrationServiceMock.create.mockImplementation(() => throwError(() => new Error('error')));
          component.submit();
          expect(bookAdministrationServiceMock.create).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, new Error('error'), expectedBook);
        }));
        test('should edit book', waitForAsync(() => {
          component.edition = true;
          bookAdministrationServiceMock.update.mockImplementation(() => throwError(() => new Error('error')));
          component.submit();
          expect(bookAdministrationServiceMock.update).toHaveBeenNthCalledWith(1, expectedBook);
          expect(spy).toHaveBeenNthCalledWith(1, new Error('error'), expectedBook);
        }));
      });
    });
    describe('IMG', () => {
      const mockCover = new File(new Array<Blob>(), 'Mock.png', {type: 'application/zip'});

      beforeEach(() => {
        component.form.patchValue({isbn: 'tata'});
        component.book = {...component.book, cover: 'init'};
      });

      test('should reset image path', () => {
        component.imagePath = 'test';
        component.resetImage();
        expect(component.getImg('toto').src.startsWith('/files/covers/')).toBeTruthy();
      });
      test('should not upload image', () => {
        component.onSelect({
          addedFiles: [],
          rejectedFiles: [],
          source: fixture.debugElement.query(By.css('ngx-dropzone')).componentInstance
        });

        expect(component.getImg('toto').src.startsWith('/files/search/covers/')).toBeTruthy();
        expect(component.uploadCover).toBeTruthy();
        expect(bookAdministrationServiceMock.uploadCover.mock.calls.length).toStrictEqual(0);
      });
      test('should upload image', waitForAsync(() => {
        bookAdministrationServiceMock.uploadCover.mockImplementation(() => of({cover: 'coverTest'}));

        component.onSelect({
          addedFiles: [mockCover],
          rejectedFiles: [],
          source: fixture.debugElement.query(By.css('ngx-dropzone')).componentInstance
        });

        expect(component.getImg('toto').src.startsWith('/files/search/covers/')).toBeTruthy();
        expect(bookAdministrationServiceMock.uploadCover).toHaveBeenNthCalledWith(1, mockCover, 'tata');
        expect(component.book.cover).toStrictEqual('coverTest');
        expect(component.uploadCover).toBeFalsy();
      }));
      test('should upload image fail', waitForAsync(() => {
        bookAdministrationServiceMock.uploadCover.mockImplementation(() => throwError(() => new Error('toto')));

        component.onSelect({
          addedFiles: [mockCover],
          rejectedFiles: [],
          source: fixture.debugElement.query(By.css('ngx-dropzone')).componentInstance
        });

        expect(component.getImg('toto').src.startsWith('/files/search/covers/')).toBeTruthy();
        expect(bookAdministrationServiceMock.uploadCover).toHaveBeenNthCalledWith(1, mockCover, 'tata');
        expect(component.book.cover).toStrictEqual('init');
        expect(component.uploadCover).toBeFalsy();
      }));
    });
  });
});

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});
