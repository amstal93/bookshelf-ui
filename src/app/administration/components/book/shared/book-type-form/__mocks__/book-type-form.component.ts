import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-book-type-form',
  template: '<div>Book Type Mocks</div>'
})
export class MockBookTypeFormComponent {

  @Input() form: any;
  @Input() appearance: any;
  @Input() classes: any;

}
