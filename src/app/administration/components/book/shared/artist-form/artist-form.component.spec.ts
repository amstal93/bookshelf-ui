import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ArtistFormComponent, ArtistServiceForm, AutoCompleteArtist} from './artist-form.component';
import {MatOptionModule} from '@angular/material/core';
import {MatChipsModule} from '@angular/material/chips';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ArtistImpl} from '../../../../../core/model/impl/artist-impl';
import {ArtistForm} from '../../../../models/form/artist-form.model';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {ArtistAdministrationService} from '../../../../services/artist-administration.service';
import {artistAdministrationServiceMock} from '../../../../services/__mocks__/artist-administration.service';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {Component, Inject, OnInit} from "@angular/core";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";
import {AutoCompleteItem} from "../../../../../shared/shared.module";
import {RestNamedEntity} from "../../../../../core/model/rest-named-entity";
import {ADMIN_SERVICE, AdministrationService} from "../../../../services/administration.service";
import {
  MockAutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/__mocks__/auto-complete-input.component";


@Component({
  selector: 'app-auto-complete-input',
  template: ` mock auto complete `
})
export class AutoCompleteInputComponent<T extends RestNamedEntity> extends MockAutoCompleteInputComponent implements OnInit {
  ngOnInit(): void {
    this.service.searchAutocomplete(...this.searchParam.getSearchParam('toto'))
  }

  constructor(
    @Inject(ADMIN_SERVICE) public service: AdministrationService<T>,
    @Inject(SEARCH_PARAM) public searchParam: AutoCompleteItem<T>
  ) {
    super()
  }
}

describe('ArtistFormComponent', () => {
  let component: ArtistFormComponent;
  let fixture: ComponentFixture<ArtistFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ArtistFormComponent,
        AutoCompleteInputComponent
      ],
      imports: [
        NoopAnimationsModule,
        FontAwesomeTestingModule,
        NgxTranslateTestingModule,
        ReactiveFormsModule,
        MatOptionModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatAutocompleteModule,
        MatInputModule
      ],
      providers: [{
        provide: ADMIN_SERVICE, useValue: artistAdministrationServiceMock,
      }, {
        provide: SEARCH_PARAM,
        useClass: AutoCompleteArtist,
        deps: [ArtistServiceForm]
      }, ArtistServiceForm
      ]
    }).overrideComponent(ArtistFormComponent, {
      set: {
        providers: [
          {provide: ArtistAdministrationService, useValue: artistAdministrationServiceMock}
        ]
      }
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistFormComponent);
    component = fixture.componentInstance;
    component.series = "series"
    component.role = "role"
    component.form = new FormGroup({...new ArtistForm(new ArtistImpl())});
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
      expect(component[`artistServiceForm`].series).toStrictEqual('series')
      expect(component[`artistServiceForm`].role).toStrictEqual('role')
      expect(artistAdministrationServiceMock.searchAutocomplete).toHaveBeenNthCalledWith(1,
        [],
        [
          {key: 'name', value: 'toto'},
          {key: 'withRolesFirst', value: 'role'},
          {key: 'withSeriesFirst', value: 'series'}
        ])
      jest.clearAllMocks();
    });
  });

  describe('Typescript test', () => {
    describe('[DELETE]', () => {
      test('emit delete event', () => {
        jest.clearAllMocks();
        const spy = jest.spyOn(component.deleteArtist, 'emit').mockImplementation(() => ({}));
        component.index = 34;
        component.delete();
        expect(spy).toHaveBeenNthCalledWith(1, 34);
      });
    });
  });
});
