import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-artist-form',
  template: '<div>Artist Form Mock</div>'
})
export class MockArtistFormComponent {

  @Input() form: any;
  @Input() index: any;
  @Output() deleteArtist: any;
}
