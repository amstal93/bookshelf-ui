import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injectable,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Artist} from '../../../../../core/model/artist';
import {ArtistAdministrationService} from '../../../../services/artist-administration.service';
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {
  AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";
import {AutoCompleteItem, SearchParam} from "../../../../../shared/shared.module";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";

@Injectable({providedIn: "root"})
export class ArtistServiceForm {
  public series: string;
  public role: string;

  setValues(
    role: string,
    series: string
  ) {
    this.series = series
    this.role = role
  }
}

type T = Artist

export class AutoCompleteArtist extends AutoCompleteItem<T> {
  constructor(
    private artistServiceForm: ArtistServiceForm
  ) {
    super();
  }

  override getSearchParam(value: string): SearchParam {
    return [
      [],
      [
        {key: 'name', value},
        {key: 'withRolesFirst', value: this.artistServiceForm.role},
        {key: 'withSeriesFirst', value: this.artistServiceForm.series}
      ]
    ]
  }
}

@Component({
  selector: 'app-artist-form',
  template: `
    <form [formGroup]="form" fxLayout="row" fxLayoutGap="1rem">
      <app-auto-complete-input
        fxFlex="100"
        [form]="form"
        [placeholder]="'BOOK.FORM.CONTRACT.ARTIST.NAME' | translate">
      </app-auto-complete-input>
      <div fxLayoutAlign="center center">
        <button type="button" color="warn" mat-icon-button matSuffix (click)="delete()">
          <fa-icon [icon]="['fas', 'trash']"></fa-icon>
        </button>
      </div>
    </form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: ADMIN_SERVICE, useClass: ArtistAdministrationService,
  }, {
    provide: SEARCH_PARAM,
    useClass: AutoCompleteArtist,
    deps: [ArtistServiceForm]
  }, ArtistServiceForm]
})
export class ArtistFormComponent implements OnInit {
  @Input() form: FormGroup;
  @Input() index: number;
  @Input() role: string;
  @Input() series: string;

  @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<Artist>;
  @Output() deleteArtist: EventEmitter<number> = new EventEmitter();

  constructor(
    private artistServiceForm: ArtistServiceForm
  ) {
  }

  ngOnInit(): void {
    this.artistServiceForm.setValues(this.role, this.series)
  }

  delete(): void {
    this.deleteArtist.emit(this.index);
  }
}
