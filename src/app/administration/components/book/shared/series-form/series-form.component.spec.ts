import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SeriesFormComponent} from './series-form.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
  NgxTranslateTestingModule
} from '../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatOptionModule} from '@angular/material/core';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SeriesAdministrationService} from '../../../../services/series-administration.service';
import {seriesAdministrationServiceMock} from '../../../../services/__mocks__/series-administration.service';

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

describe('SeriesFormComponent', () => {
  let component: SeriesFormComponent;
  let fixture: ComponentFixture<SeriesFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SeriesFormComponent],
      imports: [
        NoopAnimationsModule,
        NgxTranslateTestingModule,
        ReactiveFormsModule,
        MatOptionModule,
        MatChipsModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        MatInputModule,
        MatIconModule,
        FontAwesomeTestingModule,
        FlexLayoutModule
      ],
      providers: [
        {provide: SeriesAdministrationService, useValue: seriesAdministrationServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesFormComponent);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      id: new FormControl(),
      bookType: new FormControl(),
      name: new FormControl(),
      displayName: new FormControl()
    });
    fixture.detectChanges();
  });


  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
    test('should initialize', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('[TOOGLE MORE]', () => {
      test('from true to false', () => {
        component.display = true;
        component.toggleMore();
        expect(component.display).toBeFalsy();
      });
      test('from true to false', () => {
        component.display = false;
        component.toggleMore();
        expect(component.display).toBeTruthy();
      });
    });
    describe('[GET ICON NAME]', () => {
      test('caret up', () => {
        component.display = true;
        expect(component.getIconName()).toStrictEqual('caret-up');
      });
      test('caret down', () => {
        component.display = false;
        expect(component.getIconName()).toStrictEqual('caret-down');
      });
    });
    describe('[UPDATE]', () => {
      test('emit the new value', () => {
        const spy = jest.spyOn(component.selection, 'emit').mockImplementation(() => ({}));
        const series = {
          id: 34,
          bookType: {name: 'bookType', id: 89},
          seriesBookCount: 0,
          oneShot: false,
          name: 'name',
          displayName: 'displayName'
        }
        component.update(series);
        expect(spy).toHaveBeenNthCalledWith(1, series);
      });
    });
  });
});
