import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Series} from '../../../../../core/model/series';
import {SeriesAdministrationService} from '../../../../services/series-administration.service';
import {ADMIN_SERVICE} from "../../../../services/administration.service";
import {
  AutoCompleteInputComponent
} from "../../../../../shared/component/auto-complete-input/auto-complete-input.component";
import {SEARCH_PARAM} from "../../../../../shared/services/entity.service";
import {AutoCompleteItem} from "../../../../../shared/shared.module";

type T = Series;

class AutoCompleteSeries extends AutoCompleteItem<T> {
  override getDisplay(option: T): string {
    return `${option.name} - <small style="opacity: 0.75">&nbsp;[${option.bookType.name}] ${option.editor} </small>`
  }

  override sideEffect(form: FormGroup, value: string): void {
    form.controls.displayName.enable()
    form.controls.displayName.setValue(value)
  }
}

@Component({
  selector: 'app-series-form',
  template: `
    <form [formGroup]="form" fxLayout="column" fxFlex>
      <div fxLayout="row">
        <app-auto-complete-input
          fxFlex="100"
          [form]="form"
          [placeholder]="'BOOK.FORM.SERIES.NAME' | translate"
          (selectItem)="update($event)">
        </app-auto-complete-input>
        <div fxLayoutAlign="center center">
          <button type="button" mat-button (click)="toggleMore()">
            <fa-icon [icon]="['fas', getIconName()]"></fa-icon>
          </button>
        </div>
      </div>
      <div [fxHide]="!display">
        <mat-form-field fxFlex>
          <label>
            <input type="text"
                   [placeholder]="'BOOK.FORM.SERIES.DISPLAY_NAME' | translate"
                   formControlName="displayName"
                   matInput>
          </label>
        </mat-form-field>
      </div>
    </form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: ADMIN_SERVICE, useClass: SeriesAdministrationService
  }, {
    provide: SEARCH_PARAM, useClass: AutoCompleteSeries
  }]
})
export class SeriesFormComponent implements OnInit {

  @Input() public form: FormGroup;
  @Input() public edition: boolean = false;
  @ViewChild('appAutoCompleteInput') appAutoCompleteInput: AutoCompleteInputComponent<Series>;
  @Output() public selection: EventEmitter<Series> = new EventEmitter<Series>();

  display = false;

  ngOnInit(): void {
    if (this.edition) {
      this.form.controls.displayName.disable();
    }
  }

  update(series: Series): void {
    this.selection.emit(series);
  }

  public toggleMore(): void {
    this.display = !this.display;
  }

  /**
   * Give the good font awesome icon name
   * `caret-up` if the field display is `true`, `caret-down` otherwise
   * Return the good icon name
   */
  getIconName() {
    return this.display ? 'caret-up' : 'caret-down';
  }
}
