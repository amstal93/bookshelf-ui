import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {TaskService} from "../../../services/task.service";

@Component({
  selector: 'app-task-header-menu',
  template: `
    <ng-container *ngIf="{ val: totalTasksCount$ | async } as totalTasksCount">
      <button class="btn-icon" mat-icon-button
              [matBadge]="totalTasksCount.val"
              [disabled]="totalTasksCount.val === 0"
              [matBadgeHidden]="totalTasksCount.val === 0"
              matBadgeColor="warn"
              [matMenuTriggerFor]="menuNotification"
              aria-label="Button to display notifications">
        <fa-icon *ngIf="totalTasksCount.val > 0" [icon]="['fas', 'bell']" [size]="'2x'"></fa-icon>
        <fa-icon *ngIf="totalTasksCount.val === 0" [icon]="['far', 'bell']" [size]="'2x'"></fa-icon>
      </button>
    </ng-container>
    <mat-menu #menuNotification="matMenu" class="panelClass" xPosition="before">
      <div style="padding-left: 1rem">
        <h3 style="margin: 0"> {{'ADMINISTRATION.TASK.HEADER.TITLE' | translate}} </h3>
      </div>
      <mat-list dense="true">
        <mat-divider></mat-divider>
        <mat-list-item style="cursor: pointer"
                       [routerLink]="['administration', 'book', 'add']"
                       [queryParams]="{withMenu: true}">
          <div fxFlex fxLayout="row" fxLayoutAlign="space-between center" fxLayoutGap="3rem">
            <ng-container *ngIf="{ val: addBookTasksCount$ | async } as addBookTasksCount">
                <span [matBadge]="addBookTasksCount.val"
                      [matBadgeHidden]="addBookTasksCount.val === 0"
                      matBadgeOverlap="false"
                      matBadgePosition="above after"
                      matBadgeColor="warn">
                  {{'ADMINISTRATION.TASK.HEADER.ITEMS.ADD_BOOK' | translate}}
                </span>
              <fa-icon [icon]="['fas', 'location-arrow']"></fa-icon>
            </ng-container>
          </div>
        </mat-list-item>
      </mat-list>
    </mat-menu>
  `
})
export class TaskHeaderMenuComponent implements OnInit {


  totalTasksCount$: Observable<number> = this.taskService.count$.pipe(map(list => list['ALL'] ?? 0));
  addBookTasksCount$: Observable<number> = this.taskService.count$.pipe(map(list => list['ADD_BOOK'] ?? 0));


  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit(): void {
    this.taskService.getTaskTodoCount().subscribe()
  }

}
