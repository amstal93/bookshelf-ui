import {Component, Input} from '@angular/core';
import {WebLink} from '../../../../../core/model/web-link';
import {FormGroup} from '@angular/forms';
import TypeData = WebLink.TypeData;

@Component({
  selector: 'app-web-links-form',
  template: `
    <div fxFlex fxLayoutGap="1rem" fxLayout="row" [formGroup]="form">
      <mat-form-field appearance="outline" class="editable-field">
        <mat-select formControlName="type">
          <mat-option [value]="type.type" *ngFor="let type of types"> {{type.label}} </mat-option>
        </mat-select>
      </mat-form-field>
      <mat-form-field appearance="outline" class="editable-field">
        <input matInput formControlName="value">
      </mat-form-field>
    </div>
  `
})
export class WebLinksFormComponent {

  @Input() public types: TypeData[] = []
  @Input() public form: FormGroup;

}
