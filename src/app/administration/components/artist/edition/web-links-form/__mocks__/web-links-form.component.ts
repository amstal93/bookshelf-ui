import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-web-links-form',
  template: `<div>Mock web links form</div>`
})
export class MockWebLinksFormComponent {

  @Input() public types: any[] = []
  @Input() public form: any;

}
