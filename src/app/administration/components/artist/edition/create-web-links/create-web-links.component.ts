import {Component, EventEmitter, Input, Output} from '@angular/core';
import {WebLink} from '../../../../../core/model/web-link';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import TypeData = WebLink.TypeData;

@Component({
  selector: 'app-create-web-links',
  template: `
    <div fxLayout="row" fxLayoutGap="1rem">
      <app-web-links-form [form]="form" [types]="types"></app-web-links-form>
      <button mat-button (click)="submit()" [disabled]="!isFormValid()" color="primary">
        <fa-icon [icon]="['fas', 'check-circle']"></fa-icon>
      </button>
    </div>
  `,
})
export class CreateWebLinksComponent {

  @Input() public types: TypeData[] = []
  @Output() public create: EventEmitter<WebLink> = new EventEmitter<WebLink>()
  form: FormGroup = this.fb.group({
    type: this.fb.control(null, Validators.required),
    value: this.fb.control(null, Validators.required),
    id: null
  });

  constructor(
    private fb: FormBuilder
  ) {
  }

  submit() {
    this.create.emit(this.form.value)
    this.form.reset({type: null, value: null, id: null})
  }

  isFormValid() {
    return this.form.valid && !this.form.pristine
  }
}
