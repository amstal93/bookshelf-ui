import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BorrowerEditionListComponent} from './borrower-edition-list.component';
import {PaginationService} from "../../../../../shared/services/pagination.service";
import {paginationServiceMock} from "../../../../../shared/services/__mocks__/pagination.service";
import {BorrowerAdministrationService} from "../../../../services/borrower-administration.service";
import {borrowerAdministrationServiceMock} from "../../../../services/__mocks__/borrower-administration.service";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {
  NgxTranslateTestingModule
} from "../../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {LoanAdministrationService} from "../../../../services/loan-administration.service";
import {loanAdministrationServiceMock} from "../../../../services/__mocks__/loan-administration.service";
import {BehaviorSubject, of} from "rxjs";
import {Borrower} from "../../../../../core/model/borrower";

describe('BorrowerEditionListComponent', () => {
  let component: BorrowerEditionListComponent;
  let fixture: ComponentFixture<BorrowerEditionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BorrowerEditionListComponent],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        MatPaginatorModule,
        MatTableModule,
        ReactiveFormsModule,
        MatFormFieldModule
      ],
      providers: [
        {provide: PaginationService, useValue: paginationServiceMock},
        {provide: BorrowerAdministrationService, useValue: borrowerAdministrationServiceMock},
        {provide: LoanAdministrationService, useValue: loanAdministrationServiceMock}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerEditionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
      jest.clearAllMocks();
    });
  });

  describe('Typescript test', () => {
    describe('OnDestroy', () => {
      test('should unsub all subscription', waitForAsync(() => {
        jest.clearAllMocks();
        const obs: BehaviorSubject<number> = new BehaviorSubject<number>(0);
        let value = 0;
        component[`subscriptions`].push(obs.subscribe(v => value = v))
        obs.next(30);
        expect(value).toStrictEqual(30)
        component[`ngOnDestroy`]();
        obs.next(40);
        expect(value).toStrictEqual(30)
      }));
    });

    describe('getId', () => {
      test('should return the item id', waitForAsync(() => {
        jest.clearAllMocks();
        expect(component.getId(0, {
          id: 12,
          borrowDate: null,
          returnDate: null
        })).toStrictEqual(12)
      }));
    });

    describe('getLoans', () => {
      test('should return the loans', waitForAsync(() => {
        jest.clearAllMocks();
        const borrowDate= new Date()
        const loans = [{
          book: {
            isbn: 'bookIsbn',
            id: 0,
            title: 'title1'
          },
          borrowDate: null,
          returnDate: null
        }, {
          book: {
            isbn: 'bookIsbn2',
            id: 0,
            title: 'title1'
          },
          borrowDate,
          returnDate: null
        }]
        component.uiDatas = [{
          item: {
            id: 12,
            name: 'name',
            currentLoansCount: 0,
            loans
          },
          form: new FormGroup({}),
          isSaved: new BehaviorSubject<boolean>(false),
          start: new BehaviorSubject<boolean>(false)
        }]
        expect(component.getLoans(0)).toStrictEqual(loans)
      }));
    });

    describe('expand', () => {
      // expand(expandedElement: number, index) {
      //   const borrower = this.uiDatas[index].item
      //   this.loanAdminService.getAll(borrower.id).subscribe(loans => {
      //     this.uiDatas[index].item.loans = loans;
      //     this.expandedElement = expandedElement === index ? null : index
      //   })
      // }

      test('should expand and call the api to get all loans with details', waitForAsync(() => {
        jest.clearAllMocks();
        const borrowDate= new Date()
        const loans = [{
          book: {
            isbn: 'bookIsbn',
            id: 0,
            title: 'title1'
          },
          borrowDate: null,
          returnDate: null
        }, {
          book: {
            isbn: 'bookIsbn2',
            id: 0,
            title: 'title1'
          },
          borrowDate,
          returnDate: null
        }]
        component.uiDatas = [{
          item: {
            id: 12,
            name: 'name',
            currentLoansCount: 0,
            loans: []
          },
          form: new FormGroup({}),
          isSaved: new BehaviorSubject<boolean>(false),
          start: new BehaviorSubject<boolean>(false)
        }]
        loanAdministrationServiceMock.getAll.mockImplementation(() => of(loans))

        component.expand(13, 0)
        expect(loanAdministrationServiceMock.getAll).toHaveBeenNthCalledWith(1, 12)
        expect(component.expandedElement).toStrictEqual(0)
        expect(component.uiDatas[0].item.loans).toStrictEqual(loans)
      }));
      test('should expand and call the api to get all loans with details', waitForAsync(() => {
        jest.clearAllMocks();
        const borrowDate= new Date()
        const loans = [{
          book: {
            isbn: 'bookIsbn',
            id: 0,
            title: 'title1'
          },
          borrowDate: null,
          returnDate: null
        }, {
          book: {
            isbn: 'bookIsbn2',
            id: 0,
            title: 'title1'
          },
          borrowDate,
          returnDate: null
        }]
        component.uiDatas = [{
          item: {
            id: 12,
            name: 'name',
            currentLoansCount: 0,
            loans: []
          },
          form: new FormGroup({}),
          isSaved: new BehaviorSubject<boolean>(false),
          start: new BehaviorSubject<boolean>(false)
        }]
        loanAdministrationServiceMock.getAll.mockImplementation(() => of(loans))

        component.expand(0, 0)
        expect(loanAdministrationServiceMock.getAll).toHaveBeenCalledTimes(0)
        expect(component.expandedElement).toBeNull()
        expect(component.uiDatas[0].item.loans).toStrictEqual([])
      }));
    });

    describe('initForm', () => {
      test('should return the borrower form no web links', waitForAsync(() => {
        jest.clearAllMocks();
        const item: Borrower = {name: 'toto', loans: []}
        const form: FormGroup = new FormGroup({
          name: new FormControl(item.name)
        });
        expect(component[`initForm`](item).value).toStrictEqual(form.value)
      }));
      test('should return the borrower form with web links', waitForAsync(() => {
        jest.clearAllMocks();
        const item: Borrower = {name: 'toto'}
        const form: FormGroup = new FormGroup({
          name: new FormControl(item.name),
        });
        expect(component[`initForm`](item).value).toStrictEqual(form.value)
      }));
    });

    describe('returnLoan', () => {
      test('should return a book', waitForAsync(() => {
        const borrowDate= new Date()
        jest.clearAllMocks();
        component.uiDatas = [{
         item: {
           id: 12,
           name: 'name',
           currentLoansCount: 0,
           loans: [{
             book: {
               isbn: 'bookIsbn',
               id: 0,
               title: 'title1'
             },
             borrowDate: null,
             returnDate: null
           }, {
             book: {
               isbn: 'bookIsbn2',
               id: 0,
               title: 'title1'
             },
             borrowDate,
             returnDate: null
           }]
         },
          form: new FormGroup({}),
          isSaved: new BehaviorSubject<boolean>(false),
          start: new BehaviorSubject<boolean>(false)
        }]

        loanAdministrationServiceMock.update.mockImplementation(() => {
          return of({
            book: {
              isbn: 'toto',
              id: 0,
              title: 'title1'
            },
            borrowDate: new Date(),
            returnDate: new Date()
          })
        })
        component.returnLoan(0, 1)
        expect(loanAdministrationServiceMock.update).toHaveBeenNthCalledWith(1, {book: null, bookIsbn: "bookIsbn2", borrowDate, returnDate: expect.anything()}, false ,12)
        expect(component.uiDatas[0].item.loans.length).toStrictEqual(2)
        expect(component.uiDatas[0].item.loans[1].book.isbn).toStrictEqual('toto')
      }));
    });
    describe('deleteLoan', () => {
      test('should remove a loan', waitForAsync(() => {
        jest.clearAllMocks();
        component.uiDatas = [{
         item: {
           id: 12,
           name: 'name',
           currentLoansCount: 0,
           loans: [{
             book: {
               isbn: 'bookIsbn',
               id: 0,
               title: 'title1'
             },
             borrowDate: null,
             returnDate: null
           }, {
             book: {
               isbn: 'bookIsbn2',
               id: 0,
               title: 'title1'
             },
             borrowDate: new Date(),
             returnDate: null
           }]
         },
          form: new FormGroup({}),
          isSaved: new BehaviorSubject<boolean>(false),
          start: new BehaviorSubject<boolean>(false)
        }]

        component.deleteLoan(0, 0)
        expect(loanAdministrationServiceMock.delete).toHaveBeenNthCalledWith(1, {"book": null, "bookIsbn": "bookIsbn", "borrowDate": null, "returnDate": null}, 12)
        expect(component.uiDatas[0].item.loans.length).toStrictEqual(1)
        expect(component.uiDatas[0].item.loans[0].book.isbn).toStrictEqual('bookIsbn2')
      }));
    });
  });

});
