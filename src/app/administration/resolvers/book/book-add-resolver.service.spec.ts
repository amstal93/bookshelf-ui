import {TestBed, waitForAsync} from '@angular/core/testing';

import {BookAddResolver} from './book-add-resolver.service';
import {BookAdministrationService} from '../../services/book-administration.service';
import {bookAdministrationServiceMock} from '../../services/__mocks__/book-administration.service';
import {ActivatedRouteSnapshot} from '@angular/router';
import {BookImpl} from '../../../core/model/impl/book-impl';

describe('BookAddResolver', () => {
  let service: BookAddResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          {provide: BookAdministrationService, useValue: bookAdministrationServiceMock}
        ]
      }
    );
    service = TestBed.inject(BookAddResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should create', waitForAsync(() => {
      const route = new ActivatedRouteSnapshot();
      route.params = {isbn: '1234567890123'};

      const mockFromBookSearch = jest.fn();
      mockFromBookSearch.mockReturnValue({test: 'test'});

      BookImpl.fromBookSearch = mockFromBookSearch;

      service.resolve(route, undefined).subscribe(value =>
        expect(value).toStrictEqual({test: 'test'})
      );
      expect(bookAdministrationServiceMock.searchBook).toHaveBeenNthCalledWith(1, '1234567890123');
    }));
  });
});
