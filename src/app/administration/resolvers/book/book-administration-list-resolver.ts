import {Injectable} from '@angular/core';
import {Book} from "../../../core/model/book";
import {BookAdministrationService} from "../../services/book-administration.service";
import {ListResolver} from "../../../shared/resolvers/list-resolver";
import {SearchCriteria} from "../../../shared/services/entity.service";

@Injectable({
  providedIn: 'root'
})
export class BookAdministrationListResolver extends ListResolver<Book, BookAdministrationService> {

  constructor(
    service: BookAdministrationService
  ) {
    super(service)
  }

  protected getSearchCriteriaList(search: string): SearchCriteria[] {
    return [
      {name: 'title', operation: ':', value: `*${search}*`},
      {name: 'editor', operation: ':', value: `*${search}*`, or: true},
      {name: 'series', operation: ':', value: `*${search}*`, or: true}
    ]
  }
}
