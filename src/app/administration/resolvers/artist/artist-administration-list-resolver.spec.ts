import {ArtistAdministrationListResolver} from "./artist-administration-list-resolver";
import {TestBed} from "@angular/core/testing";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {PaginationService} from "../../../shared/services/pagination.service";
import {ArtistAdministrationService} from "../../services/artist-administration.service";
import {artistAdministrationServiceMock} from "../../services/__mocks__/artist-administration.service";

describe('ArtistAdministrationListResolver', () => {
  let service: ArtistAdministrationListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ArtistAdministrationListResolver,
        {provide: ArtistAdministrationService, useValue: artistAdministrationServiceMock},
        {provide: PaginationService, useValue: paginationServiceMock}
      ]
    });
    service = TestBed.inject(ArtistAdministrationListResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
});
