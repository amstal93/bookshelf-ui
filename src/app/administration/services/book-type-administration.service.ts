import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {AdministrationService} from './administration.service';
import {BookType} from '../../core/model/book-type';

@Injectable({
  providedIn: 'root'
})
export class BookTypeAdministrationService extends AdministrationService<BookType> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'bookTypes');
  }
}
