import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BookForm} from '../models/form/book-form.model';
import {BookImpl} from '../../core/model/impl/book-impl';
import {Book} from '../../core/model/book';
import {BehaviorSubject, Observable} from 'rxjs';
import {ContractFrom} from '../models/form/contract.from.model';
import {Role} from '../../core/model/role';
import {Artist} from '../../core/model/artist';
import {ArtistForm} from '../models/form/artist-form.model';
import {Series} from "../../core/model/series";
import {Editor} from "../../core/model/editor";

@Injectable({
  providedIn: 'root'
})
export class BookFormService {

  private readonly contractControlName = 'contracts';

  private book: Book;

  private bookForm: BehaviorSubject<FormGroup | undefined> = new BehaviorSubject(this.fb.group(new BookForm(new BookImpl())));


  get bookForm$(): Observable<FormGroup> {
    return this.bookForm.asObservable();
  }

  constructor(
    private fb: FormBuilder
  ) {
  }

  initForm(book: Book): void {
    this.book = book;
    this.bookForm.next(this.fb.group(new BookForm(book)));
    this.toggleOneShot(book.series.oneShot);
  }

  getContractRoles(): Role[] {
    const book = this.bookForm.getValue();
    return (book.get(this.contractControlName) as FormArray).value.map(value => value.role)
  }

  addContract(role: Role): void {
    const book = this.bookForm.getValue();
    (book.get(this.contractControlName) as FormArray)
      .push(this.fb.group(new ContractFrom({artists: [], role})));

    this.bookForm.next(book);
  }

  addArtist(contractIndex: number, artist: Artist): void {
    const book = this.bookForm.getValue();
    ((book.get(this.contractControlName) as FormArray).controls[contractIndex].get('artists') as FormArray)
      .push(this.fb.group(new ArtistForm(artist)));

    this.bookForm.next(book);
  }

  toggleOneShot(setOneShot: boolean): void {
    const form = this.bookForm.value;
    let formValue;

    if (setOneShot) {
      form.controls.tome.disable();
      form.controls.tome.reset();
      form.controls.series.disable();
      (form.controls.series as FormGroup).controls.bookType.enable();
      formValue = {
        series: {
          bookType: this.book.series.bookType,
          name: form.value.title,
          displayName: form.value.title
        }
      };
    } else {
      form.controls.tome.enable();
      form.controls.series.enable();
      formValue = {
        tome: this.book.tome,
        series: {
          bookType: this.book.series.bookType,
          name: this.book.series.name,
          displayName: this.book.series.displayName
        }
      };
    }

    form.patchValue(formValue, { emitEvent: false });
    this.bookForm.next(form);
  }

  deleteContract(i: number): void {
    const book = this.bookForm.getValue();
    (book.get(this.contractControlName) as FormArray)
      .removeAt(i);

    this.bookForm.next(book);
  }

  deleteArtist(contractIndex: number, indexArtist: number): void {
    const book = this.bookForm.getValue();
    ((book.get(this.contractControlName) as FormArray).controls[contractIndex].get('artists') as FormArray)
      .removeAt(indexArtist);

    this.bookForm.next(book);
  }

  updateSeries(series: Series): void {
    const form = this.bookForm.value;

    if (series.id) {
      (form.controls.series as FormGroup).controls.displayName.disable();
    }

    form.patchValue({
      series: {
        id: series.id,
        bookType: series.bookType,
        name: series.name,
        displayName: series.displayName
      },
      editor: {
        id: null,
        name: series.editor
      }
    }, {emitEvent: false});

    this.bookForm.next(form);
  }

  updateEditor(editor: Editor): void {
    const form = this.bookForm.value;

    form.patchValue({
      editor: {
        id: editor.id,
        name: editor.name
      }
    }, {emitEvent: false});

    this.bookForm.next(form);
  }

  getCurrentSeries(): Series {
    return this.bookForm.value.get('series').value;
  }

  updateOfferedBookStatus(offered: boolean): void {
    const form = this.bookForm.value;

    form.patchValue({
      metadata: {
        offered,
        price: null,
        priceCurrency: null
      }
    }, {emitEvent: false});

    this.bookForm.next(form);
  }

  setCurrencyMandatory(price: number): void {
    const form = this.bookForm.value;
    const priceCurrencyControl = form.get("metadata.priceCurrency")
    if (price === null) {
      priceCurrencyControl.removeValidators(Validators.required)
      priceCurrencyControl.setValue(null)
    } else if (price >= 0)  {
      priceCurrencyControl.addValidators(Validators.required)
    }
    this.bookForm.next(form);
  }
}
