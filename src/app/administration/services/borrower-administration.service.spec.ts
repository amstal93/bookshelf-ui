import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {BorrowerAdministrationService} from "./borrower-administration.service";

describe('BorrowerAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: BorrowerAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(BorrowerAdministrationService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['borrowers']);
      expect(service).toBeTruthy();
    });
  });
});
