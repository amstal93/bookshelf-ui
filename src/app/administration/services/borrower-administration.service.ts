import {Injectable} from '@angular/core';
import {AdministrationService} from './administration.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {Borrower} from "../../core/model/borrower";

@Injectable({
  providedIn: 'root'
})
export class BorrowerAdministrationService extends AdministrationService<Borrower> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'borrowers');
  }
}
