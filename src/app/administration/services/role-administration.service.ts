import {Injectable} from '@angular/core';
import {AdministrationService} from './administration.service';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {Role} from '../../core/model/role';

@Injectable({
  providedIn: 'root'
})
export class RoleAdministrationService extends AdministrationService<Role> {

  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, 'roles');
  }
}
