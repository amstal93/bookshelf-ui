import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {LoanAdministrationService} from "./loan-administration.service";

describe('LoanAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: LoanAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(LoanAdministrationService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['borrowers', 'loans']);
      expect(service).toBeTruthy();
    });
  });
});
