import {BehaviorSubject, of} from 'rxjs';
import fn = jest.fn;

export const taskServiceMock = {
  list: of([]),
  count$: new BehaviorSubject(2),
  listTodo$: new BehaviorSubject([]),
  delete: fn(() => of({
      list: [],
      currentPage: 0,
      totalPages: 0,
      totalElements: 0
    })
  ),
  getTaskTodoCount: fn(() => of({ALL: 0})),
  getTaskTodo: fn(() => of({
    list: [],
    currentPage: 0,
    totalPages: 0,
    totalElements: 0
  }))
};
