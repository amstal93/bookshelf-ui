import {of} from 'rxjs';
import fn = jest.fn;

export const editorAdministrationServiceMock = {
  list: of([]),
  getAll: fn(() => of([])),
  delete: fn((bookType: any) => {}),
  update: fn((bookType: any) => of(bookType)),
  search: fn((search: string) => of([])),
  searchWithOption: fn(() => of({body:[]}))
};
