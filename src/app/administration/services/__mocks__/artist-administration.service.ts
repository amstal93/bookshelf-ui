import fn = jest.fn;
import {of} from 'rxjs';
import {HttpResponse} from "@angular/common/http";

export const artistAdministrationServiceMock = {
  list$: of([]),
  search: fn(() => of(new HttpResponse( {body: []}))),
  searchAutocomplete: fn(() => of(new HttpResponse( {body: []}))),
};
