import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Task, TaskStatusEnum, TaskTypeEnum} from "../models/task";
import {BehaviorSubject, Observable, of} from "rxjs";
import {catchError, switchMap, tap} from "rxjs/operators";
import {TaskCount} from "../models/task-count";
import {Page} from "../../core/model/page";

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private apiEndpoint: string = 'tasks'

  protected count: BehaviorSubject<TaskCount> = new BehaviorSubject<TaskCount>({ALL: 0});

  get count$(): Observable<TaskCount> {
    return this.count.asObservable();
  }

  protected listTodo: BehaviorSubject<Task[]> = new BehaviorSubject<Task[]>([]);

  get listTodo$(): Observable<Task[]> {
    return this.listTodo.asObservable();
  }

  constructor(
    private http: HttpClient
  ) {
  }

  getTaskTodoCount(): Observable<TaskCount> {
    return this.http.get<TaskCount>(`/api/${this.apiEndpoint}/${TaskStatusEnum.TODO}/count`).pipe(
      tap(res => this.count.next(res)),
      catchError(err => {
        console.error('an error occured!', err)
        return of({ALL: 0})
      })
    );
  }

  getTaskTodo(): Observable<Page<Task>> {
    this.getTaskTodoCount().subscribe()
    return this.http.get<Page<Task>>(`/api/${this.apiEndpoint}/${TaskTypeEnum.ADD_BOOK}/${TaskStatusEnum.TODO}`).pipe(
      tap(res => this.listTodo.next(res.list)),
      catchError(err => {
        console.error('an error occured!', err)
        return of({
          list: [],
          currentPage: 0,
          totalPages: 0,
          totalElements: 0
        })
      })
    );
  }

  delete(task: Task): Observable<Page<Task>> {
    return this.http.delete(`/api/${this.apiEndpoint}/${task.id}`).pipe(
      catchError(err => {
        console.error('an error occured!', err)
        throw err;
      }),
      switchMap(() => this.getTaskTodo())
    );
  }
}
