import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {SeriesAdministrationService} from './series-administration.service';
import {SeriesImpl} from '../../core/model/impl/series-impl';


const mockData1 = new SeriesImpl();
mockData1.name = 'TEST';
mockData1.seriesBookCount = 1;

const mockData2 = new SeriesImpl();
mockData2.name = 'totototo';
mockData2.seriesBookCount = 0;

describe('SeriesAdministrationService', () => {
  let httpTestingController: HttpTestingController;
  let service: SeriesAdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(SeriesAdministrationService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['series']);
      expect(service).toBeTruthy();
    });
  });
});
