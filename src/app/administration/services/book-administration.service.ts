import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {BookSearch} from '../models/book-search';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {Book} from '../../core/model/book';
import {CoreService} from '../../core/services/core.service';
import {AdministrationService} from './administration.service';
import {MatDialog} from '@angular/material/dialog';
import {DialogConfirmComponent} from '../../shared/confirm-modal/dialog-confirm.component';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class BookAdministrationService extends AdministrationService<Book> {

  constructor(
    http: HttpClient,
    coreService: CoreService,
    private dialog: MatDialog,
    private translateService: TranslateService
  ) {
    super(http, coreService, 'books');
  }

  get searchResult$(): Observable<BookSearch | null> {
    return this.searchResult.asObservable();
  }

  private searchResult: BehaviorSubject<BookSearch | null> = new BehaviorSubject<BookSearch | null>(null);

  protected getId(book: Book): string | number {
    return book.isbn;
  }

  searchBook(isbn: string): Observable<BookSearch> {
    this.coreService.updateLoadingState(true);
    const params = new HttpParams()
      .append('isbn', isbn);
    return this.http.get<BookSearch>('/bnf-api/search/books/findByISBN', {params}).pipe(
      catchError((error) => {
        this.coreService.updateLoadingState(false);
        return this.openDialog(isbn).pipe(switchMap(res => {
          this.coreService.updateLoadingState(false);
          if (res) {
              return of({
                arkId: 'manual',
                authors: [],
                collection: null,
                cover: null,
                editor: null,
                isbn,
                rolesByAuthors: [],
                series: null,
                title: null,
                tome: null,
                year: null
              });
            } else {
              throw error;
            }
          }
        ));
      }),
      tap(res => {
        this.searchResult.next(res);
        this.coreService.updateLoadingState(false);
      }));
  }

  private openDialog(isbn: string): Observable<boolean> {
    return this.dialog.open(DialogConfirmComponent, {
      width: '450px',
      data: {
        title: this.translateService.instant('BOOK.SEARCH.ERRORS.NO_RESULT_ISBN.TITLE'),
        message: this.translateService.instant('BOOK.SEARCH.ERRORS.NO_RESULT_ISBN.MESSAGE', {isbn}),
        question: this.translateService.instant('BOOK.SEARCH.ERRORS.NO_RESULT_ISBN.QUESTION')
      }
    }).afterClosed();
  }

  clearResults() {
    this.searchResult.next(null);
  }

  uploadCover(cover: File, isbn: any): Observable<{ cover: string }> {
    const formData: FormData = new FormData();
    formData.append('file', cover);
    return this.http.put<{ cover: string }>(`/api/books/${isbn}/cover`, formData);
  }
}
