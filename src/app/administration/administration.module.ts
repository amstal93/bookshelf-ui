import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {BookAddComponent} from './components/book/creation/book-add/book-add.component';
import {FormsModule} from '@angular/forms';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSelectModule} from '@angular/material/select';
import {
  SeriesEditionListComponent
} from './components/series/edition/series-edition-list/series-edition-list.component';
import {BookEditionListComponent} from './components/book/edition/book-edition-list/book-edition-list.component';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BookTypeGestionComponent} from './components/bookType/gestion/book-type-gestion/book-type-gestion.component';
import {
  BookTypeGestionListComponent
} from './components/bookType/gestion/book-type-gestion-list/book-type-gestion-list.component';
import {
  BookTypeCreationListDisplayComponent
} from './components/bookType/gestion/book-type-creation-list-display/book-type-creation-list-display.component';
import {
  BookTypeEditionListDisplayComponent
} from './components/bookType/gestion/book-type-edition-list-display/book-type-edition-list-display.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {BookFormComponent} from './components/book/shared/book-form/book-form.component';
import {ArtistFormComponent} from './components/book/shared/artist-form/artist-form.component';
import {EditorFormComponent} from './components/book/shared/editor-form/editor-form.component';
import {SeriesFormComponent} from './components/book/shared/series-form/series-form.component';
import {BookTypeFormComponent} from './components/book/shared/book-type-form/book-type-form.component';
import {MatChipsModule} from '@angular/material/chips';
import {BookAddResolver} from './resolvers/book/book-add-resolver.service';
import {BookEditComponent} from './components/book/edition/book-edit/book-edit.component';
import {NgxFileDropModule} from 'ngx-file-drop';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {
  faBars,
  faCheckCircle,
  faEyeSlash,
  faPlusCircle,
  faTimes,
  faTimesCircle
} from '@fortawesome/free-solid-svg-icons';
import {ContractFormComponent} from './components/book/shared/contract-form/contract-form.component';
import {RoleFormComponent} from './components/book/shared/role-form/role-form.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {
  ArtistEditionListComponent
} from './components/artist/edition/artist-edition-list/artist-edition-list.component';
import {WebLinksFormComponent} from './components/artist/edition/web-links-form/web-links-form.component';
import {CreateWebLinksComponent} from './components/artist/edition/create-web-links/create-web-links.component';
import {AdministrationPageComponent} from './pages/administration-page/administration-page.component';
import {MatCardModule} from "@angular/material/card";
import {TaskHeaderMenuComponent} from './components/tasks/task-header-menu/task-header-menu.component';
import {MatMenuModule} from "@angular/material/menu";
import {TaskMenuComponent} from './components/tasks/task-menu/task-menu.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {EditionComponent} from "./pages/edition/edition.component";
import {AdministrationRoutingModule} from "./administration-routing.module";
import {
  BorrowerEditionListComponent
} from './components/borrower/edition/borrower-edition-list/borrower-edition-list.component';
import {BookMetadataFormComponent} from './components/book/shared/book-metadata-form/book-metadata-form.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatTabsModule} from "@angular/material/tabs";
import {
  DateInputAcquisitionComponent
} from './components/book/shared/book-metadata-form/date-input-acquisition/date-input-acquisition.component';
import {
  DateInputOriginalReleaseComponent
} from './components/book/shared/book-metadata-form/date-input-original-release/date-input-original-release.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    AdministrationRoutingModule,
    MatListModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
    FontAwesomeModule,
    MatSlideToggleModule,
    MatChipsModule,
    NgxFileDropModule,
    NgxDropzoneModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatMenuModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatTabsModule
  ],
  declarations: [
    BookAddComponent,
    SeriesEditionListComponent,
    BookEditionListComponent,
    BookTypeGestionComponent,
    BookTypeGestionListComponent,
    BookTypeEditionListDisplayComponent,
    BookTypeCreationListDisplayComponent,
    BookFormComponent,
    ArtistFormComponent,
    EditorFormComponent,
    SeriesFormComponent,
    BookTypeFormComponent,
    BookEditComponent,
    ContractFormComponent,
    RoleFormComponent,
    ArtistEditionListComponent,
    WebLinksFormComponent,
    CreateWebLinksComponent,
    AdministrationPageComponent,
    TaskHeaderMenuComponent,
    EditionComponent,
    TaskMenuComponent,
    BorrowerEditionListComponent,
    BookMetadataFormComponent,
    DateInputAcquisitionComponent,
    DateInputOriginalReleaseComponent
  ],
  exports: [
    TaskHeaderMenuComponent
  ],
  providers: [
    BookAddResolver
  ]
})
export class AdministrationModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faPlusCircle,
      faTimesCircle,
      faTimes,
      faBars,
      faCheckCircle,
      faEyeSlash
    );
    // Add an icon to the library for convenient access in other components
  }
}
