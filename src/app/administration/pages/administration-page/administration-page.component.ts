import {Component} from '@angular/core';

@Component({
  selector: 'app-administration-page',
  template: `
    <div fxLayout="row" class="content">
      <app-task-menu [fxFlex]="'none'" (updateWidth)="menuWith = $event"></app-task-menu>
      <div fxFlex>
        <router-outlet></router-outlet>
      </div>
    </div>
  `
})
export class AdministrationPageComponent {
  menuWith?;
}

