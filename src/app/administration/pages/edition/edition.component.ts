import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Data, NavigationEnd, Router} from "@angular/router";
import {PaginationService} from "../../../shared/services/pagination.service";
import {filter, map, mergeMap} from "rxjs/operators";
import {QueryParams} from "../../../artist/pages/artist-list-page/artist-list-page.component";

@Component({
  selector: 'app-edition',
  template: `
    <div fxLayout="column" class="content">
      <div fxLayout="row" fxLayoutAlign="center">
        <h1 style="text-align: center">{{ translationRootKey + '.EDITION.TITLE' | translate}}</h1>
      </div>
      <div fxLayout="row" fxLayoutAlign="space-around">
        <app-list-filter fxFlex="100"
                         [initFilter$]="filterStr$"
                         (filter)="onFilter($event)"
                         [translateKey]="translationRootKey + '.EDITION.FILTER_PLACEHOLDER'">
        </app-list-filter>
      </div>
      <router-outlet fxFlex="100"></router-outlet>
    </div>
  `
})
export class EditionComponent implements OnInit {

  public filterStr$ = this.paginationService.filterStr$
  public translationRootKey: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private paginationService: PaginationService
  ) {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.route),
        map((activatedRoute: ActivatedRoute) => {
          while (activatedRoute.firstChild) activatedRoute = activatedRoute.firstChild;
          return activatedRoute;
        }),
        mergeMap((route: ActivatedRoute) => route.data),
      )
      .subscribe({
        next: (data: Data) => {
          this.paginationService.updatePaginationAndFilter(data.resolvedData)
          this.translationRootKey = data.translationRootKey
        }
      });
  }

  ngOnInit(): void {
    this.paginationService.newPageEvent$.pipe(filter(next => next && next !== undefined)).subscribe(newPage => this.onNewPage(newPage))
  }

  onFilter(search: string) {
    this.updateQueryParams({search, page: 0})
  }

  onNewPage(pageEvent: { page: number, size: number }): void {
    this.updateQueryParams({page: pageEvent.page, size: pageEvent.size})
  }

  updateQueryParams(queryParams: QueryParams) {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams,
        queryParamsHandling: 'merge',
      }
    );
  }
}
