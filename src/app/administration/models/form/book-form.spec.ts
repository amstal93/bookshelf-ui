import {BookForm} from './book-form.model';
import {SeriesImpl} from '../../../core/model/impl/series-impl';
import moment from "moment";

describe('BookForm', () => {
  test('should create an empty instance ', () => {
    const bookForm: BookForm = new BookForm(
      {
        arkId: undefined,
        contracts: undefined,
        collection: undefined,
        cover: undefined,
        editor: undefined,
        id: 0,
        isbn: undefined,
        series: undefined,
        status: undefined,
        title: undefined,
        tome: undefined,
        year: undefined,
        metadata: null
      }
    );
    expect(bookForm).toBeTruthy();
    expect(bookForm.arkId.value).toBeNull();
    expect(bookForm.contracts.value).toStrictEqual([]);
    expect(bookForm.collection.value).toBeNull();
    expect(bookForm.editor.value).toStrictEqual({name: null, id: null});
    expect(bookForm.isbn.value).toBeNull();
    expect(bookForm.series.value).toStrictEqual({displayName: null, name: null, bookType: {name: null, id: null}, id: null});
    expect(bookForm.title.value).toStrictEqual(null);
    expect(bookForm.tome.value).toStrictEqual(null);
    expect(bookForm.year.value).toBeNull();
  });
  test('should create an instance ', () => {
    const series = new SeriesImpl('s');
    series.bookType = {name: 'BD', id: null}
    const bookForm: BookForm = new BookForm(
      {
        arkId: 'arkId',
        contracts: [],
        collection: 'collection',
        cover: undefined,
        editor: {name: 'e', id: 23},
        id: 0,
        isbn: '1234567890123',
        series,
        status: undefined,
        title: 'title',
        tome: 'tome',
        year: 'year',
        metadata: {
          acquisitionDate: '2022/11/30',
          offered: true,
          originalReleaseDate: '2205/04',
          pageCount: 123,
          price: 456,
          priceCurrency: 'EUR'
        }
      }
    );
    expect(bookForm).toBeTruthy();
    expect(bookForm.arkId.value).toStrictEqual('arkId');
    expect(bookForm.contracts.value).toStrictEqual([]);
    expect(bookForm.collection.value).toStrictEqual('collection');
    expect(bookForm.editor.value).toStrictEqual({name: 'e', id: 23});
    expect(bookForm.isbn.value).toStrictEqual('1234567890123');
    expect(bookForm.series.value).toStrictEqual({displayName: 's', name: 's', bookType: {name: 'BD', id: null}, id: null});
    expect(bookForm.title.value).toStrictEqual('title');
    expect(bookForm.tome.value).toStrictEqual('tome');
    expect(bookForm.year.value).toStrictEqual('year');
    expect(bookForm.metadata.value).toStrictEqual( {
      acquisitionDate: moment(["2022", "11", "30"]),
      offered: true,
      originalReleaseDate: moment(["2205", "04"]),
      pageCount: 123,
      price: 456,
      priceCurrency: 'EUR'
    });
  });
});
