import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Contract} from '../../../core/model/contract';
import {ArtistForm} from './artist-form.model';

export class ContractFrom {
  artists = new FormArray([], Validators.required);
  role = new FormGroup({
    id: new FormControl(),
    name: new FormControl()
  }, Validators.required);

  constructor(contract: Contract) {
    contract.artists.forEach(artist => this.artists.push(new FormGroup({...new ArtistForm(artist)})));
    this.role.setValue({
      id: contract.role.id ?? null,
      name: contract.role.name
    });
  }
}
