import {FormControl, Validators} from '@angular/forms';
import {Artist} from '../../../core/model/artist';

export class ArtistForm {
  name = new FormControl('', Validators.required);
  id = new FormControl(null);

  constructor(artist: Artist) {
    this.name.setValue(artist.name);
    this.id.setValue(artist.id);
  }
}
