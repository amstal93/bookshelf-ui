import {ContractFrom} from './contract.from.model';

describe('ContractFrom', () => {
  test('should create an instance', () => {
    const contractFromModel: ContractFrom = new ContractFrom({artists: [], role: {name: 'role1'}});
    expect(contractFromModel).toBeTruthy();
    expect(contractFromModel.artists.value).toStrictEqual([]);
    expect(contractFromModel.role.value).toStrictEqual({id: null, name: 'role1'});
  });

  test('should create an instance', () => {
    const contractFromModel: ContractFrom = new ContractFrom({artists: [], role: {id: 9, name: 'role1'}});
    expect(contractFromModel).toBeTruthy();
    expect(contractFromModel.artists.value).toStrictEqual([]);
    expect(contractFromModel.role.value).toStrictEqual({id: 9, name: 'role1'});
  });
});
