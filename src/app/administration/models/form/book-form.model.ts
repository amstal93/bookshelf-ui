import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Book} from '../../../core/model/book';
import {ContractFrom} from './contract.from.model';
import moment from "moment";

export class BookForm {
  title = new FormControl(null, [Validators.required]);
  tome = new FormControl(null, [Validators.min(0)]);
  arkId = new FormControl(null);
  isbn = new FormControl(null, [Validators.required]);
  year = new FormControl(null);
  collection = new FormControl(null);
  series = new FormGroup({
    id: new FormControl(null),
    bookType: new FormGroup({
      id: new FormControl(null),
      name: new FormControl(null, [Validators.required])
    }),
    name: new FormControl(null, [Validators.required]),
    displayName: new FormControl(null)
  });
  editor = new FormGroup({
    id: new FormControl(null),
    name: new FormControl(null, [Validators.required])
  });
  metadata = new FormGroup({
    acquisitionDate: new FormControl(null),
    offered: new FormControl(false),
    originalReleaseDate: new FormControl(null),
    pageCount: new FormControl(null, Validators.min(0)),
    price: new FormControl(null, [Validators.min(0), Validators.pattern(/^\d*(\.\d{0,2})?$/)]),
    priceCurrency: new FormControl(null)
  });
  contracts = new FormArray([]);

  constructor(
    book: Book
  ) {
    // Init form logic
    if (book.title) {
      this.title.setValue(book.title);
    }
    if (book.tome) {
      this.tome.setValue(book.tome);
    }
    if (book.arkId) {
      this.arkId.setValue(book.arkId);
    }
    if (book.isbn) {
      this.isbn.setValue(book.isbn);
    }
    if (book.year) {
      this.year.setValue(book.year);
    }
    if (book.collection) {
      this.collection.setValue(book.collection);
    }
    if (book.series) {
      this.series.setValue({
        id: book.series.id,
        bookType: {
          id: book.series.bookType?.id ?? null,
          name: book.series.bookType?.name ?? null
        },
        name: book.series.name,
        displayName: book.series.displayName,
      });
    }
    if (book.editor) {
      this.editor.setValue({
        id: book.editor.id,
        name: book.editor.name
      });
    }
    if (book.metadata) {
      this.metadata.setValue({
        acquisitionDate: moment(book.metadata.acquisitionDate.split("/")),
        offered: book.metadata.offered,
        originalReleaseDate: moment(book.metadata.originalReleaseDate.split("/")),
        pageCount: book.metadata.pageCount,
        price: book.metadata.price,
        priceCurrency: book.metadata.priceCurrency,
      });
    }

    if (book.contracts) {
      book.contracts.forEach(contract => this.contracts.push(new FormGroup({...new ContractFrom(contract)})));
    }
  }
}
