import {ArtistForm} from './artist-form.model';

describe('ArtistForm', () => {
  test('should create an instance', () => {
    const authorForm: ArtistForm = new ArtistForm({id: 10, webLinks: [], name: 'nameArtist'});
    expect(authorForm).toBeTruthy();
    expect(authorForm.name.value).toStrictEqual('nameArtist');
  });
});
