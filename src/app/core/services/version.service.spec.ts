import {TestBed, waitForAsync} from '@angular/core/testing';

import {VersionService} from './version.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('VersionService', () => {
  let httpTestingController: HttpTestingController;
  let service: VersionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(VersionService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('getUiAndGlobalVersion', waitForAsync(() => {
      service.getUiAndGlobalVersion();
      service.globalVersion$.subscribe(version => expect(version).toStrictEqual(null));
      service.uiVersion$.subscribe(version => expect(version).toStrictEqual(null));
      expect(service).toBeTruthy();
    }));
  });
});
