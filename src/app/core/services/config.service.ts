import {Injectable} from '@angular/core';
import {AppConfig} from '../model/app-config';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private appConfig = new BehaviorSubject<AppConfig | null>(null);
  get appConfig$() {
    return this.appConfig.asObservable();
  }

  setAppConfig(appConfig: AppConfig) {
    this.appConfig.next(appConfig);
  }
}
