import {TestBed, waitForAsync} from '@angular/core/testing';

import {ConfigService} from './config.service';

describe('ConfigService', () => {
  let service: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript tests', () => {
    test('should update config', waitForAsync(() => {
      service.setAppConfig({
        oauth: null,
        versionInformation: null,
        globalVersion: null,
        urlScanner: null
      });
      service.appConfig$
        .subscribe((appConfig) => expect(appConfig)
          .toStrictEqual({oauth: null, versionInformation: null, globalVersion: null,
            urlScanner: null}));
    }));
  });
});
