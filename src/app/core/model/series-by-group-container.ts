import {Book} from './book';

export interface SeriesInfo {
    seriesBookCount: number;
    books: Book[];
    oneShot: boolean;
}

export type SeriesByGroupContainer = Map<string, BookBySeriesContainer>;
export type BookBySeriesContainer = Map<string, SeriesInfo>;
