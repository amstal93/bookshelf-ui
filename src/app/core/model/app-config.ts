import {VersionInformation} from './version-information';

export interface AppConfig {
  oauth: OAuthConfig;
  versionInformation: VersionInformation;
  globalVersion: string;
  urlScanner: string;
}

export interface OAuthConfig {
  stsServer: string;
  clientId: string;
  silentRenew: boolean;
  renewTimeBeforeTokenExpiresInSeconds: number;
}
