import {RestEntity} from './rest-entity';
import {IconName} from '@fortawesome/fontawesome-common-types';
import {IconPrefix} from '@fortawesome/free-solid-svg-icons';

export interface WebLink extends RestEntity {
  value: string;
  type: WebLink.TypeEnum;
}

export namespace WebLink {
  export type TypeEnum = 'TWITTER' | 'ART_STATION' | 'FACEBOOK' | 'INSTAGRAM' | 'DEVIANT_ART' | 'OTHER';
  export const TypeEnum = {
    TWITTER: 'TWITTER' as TypeEnum,
    ART_STATION: 'ART_STATION' as TypeEnum,
    FACEBOOK: 'FACEBOOK' as TypeEnum,
    INSTAGRAM: 'INSTAGRAM' as TypeEnum,
    DEVIANT_ART: 'DEVIANT_ART' as TypeEnum,
    OTHER: 'OTHER' as TypeEnum
  };

  export interface TypeData {
    type: TypeEnum,
    label: string;
    urlPrefix: string;
    icon: [IconPrefix, IconName];
  }

  export function values(): string[] {
    return Object.keys(TypeEnum);
  }

  export function typeDatas(): TypeData[] {
    return Object.keys(TypeEnum).map(typeData);
  }

  export function typeData(type: WebLink.TypeEnum): TypeData {
    switch (type) {
      case 'DEVIANT_ART':
        return {
          type,
          label: 'Deviant Art',
          urlPrefix: 'https://www.deviantart.com/',
          icon: ['fab', 'deviantart']
        }
      case 'ART_STATION':
        return {
          type,
          label: 'Art Station',
          urlPrefix: 'https://www.artstation.com/',
          icon: ['fab', 'artstation']
        }
      case 'FACEBOOK':
        return {
          type,
          label: 'Facebook',
          urlPrefix: 'https://www.facebook.com/',
          icon: ['fab', 'facebook']
        }
      case 'INSTAGRAM':
        return {
          type,
          label: 'Instagram',
          urlPrefix: 'https://www.instagram.com/',
          icon: ['fab', 'instagram']
        }
      case 'TWITTER':
        return {
          type,
          label: 'Twitter',
          urlPrefix: 'https://twitter.com/',
          icon: ['fab', 'twitter']
        }
      case 'OTHER':
      default:
        return {
          type,
          label: 'Site Web',
          urlPrefix: '',
          icon: ['fas', 'link']
        }
    }
  }
}
