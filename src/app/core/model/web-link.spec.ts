import {WebLink} from "./web-link";

describe('WebLinks', () => {

  test('values', () => {
    expect(WebLink.values())
      .toStrictEqual([
        'TWITTER',
        'ART_STATION',
        'FACEBOOK',
        'INSTAGRAM',
        'DEVIANT_ART',
        'OTHER'
      ])
  });
});
