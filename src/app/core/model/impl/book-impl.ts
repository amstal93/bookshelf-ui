import {Book} from '../book';
import {Editor} from '../editor';
import {EditorImpl} from './editor-impl';
import {Series} from '../series';
import {SeriesImpl} from './series-impl';
import {Artist} from '../artist';
import {BookSearch} from '../../../administration/models/book-search';
import {Contract} from '../contract';
import {ContractImpl} from './contract-impl';
import {Role} from '../role';
import {idType} from "../rest-entity";
import {BookMetadata} from "../book-metadata";

export class BookImpl implements Book {
  public id: idType;
  public arkId: string;
  public collection: string;
  public cover: string;
  public isbn: string;
  public tome: string;
  public year: string;
  public metadata: BookMetadata;

  constructor(
    public title: string = '',
    public editor: Editor = new EditorImpl(),
    public series: Series = new SeriesImpl(),
    public contracts: Contract[] = [],
    public status: Book.StatusEnum = 'UNREAD',
  ) {
  }

  static fromBook(book: Book): BookImpl {
    return {
      ...new BookImpl(
        book.title,
        EditorImpl.fromEditor(book.editor),
        SeriesImpl.fromSeries(book.series),
        book.contracts.map(contract => ContractImpl.fromContract(contract)),
        book.status
      ),
      arkId: book.arkId,
      collection: book.collection,
      cover: book.cover,
      isbn: book.isbn,
      tome: book.tome,
      year: book.year,
      metadata: book.metadata
    };
  }

  static fromBookSearch(bookSearch: BookSearch): Book {
    const contracts: Contract[] = [];
    const mapRoleArtist: Map<Role, Artist[]> = new Map<Role, Artist[]>();
    (bookSearch.authors || []).forEach(authorSearch => {
      ContractImpl.fromAuthorSearch(authorSearch).forEach(contract  =>  {
        let key = null;
        for (const k of mapRoleArtist.keys()) {
          if (k.name === contract.role.name) {
            key = k;
            break;
          }
        }
        if (key) {
          mapRoleArtist.get(key).push(...contract.artists);
        } else {
          mapRoleArtist.set(contract.role, contract.artists);
        }
      });
    });
    for (const [key, value] of mapRoleArtist.entries()) {
      contracts.push(new ContractImpl( {...key}, value));
    }
    const book = new BookImpl(
      bookSearch.title,
      EditorImpl.fromEditorSearch(bookSearch.editor),
      SeriesImpl.fromSeriesSearch(bookSearch.series),
      contracts
    );
    book.arkId = bookSearch.arkId;
    book.collection = bookSearch.collection;
    book.cover = bookSearch.cover;
    book.isbn = bookSearch.isbn;
    book.tome = bookSearch.tome;
    book.year = bookSearch.year;
    return book;
  }
}
