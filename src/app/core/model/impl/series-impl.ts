import {Series} from '../series';
import {idType} from "../rest-entity";
import {BookType} from "../book-type";

export class SeriesImpl implements Series {

  id?: idType;
  seriesBookCount: number;

  constructor(
    public name: string = '',
    public editor = null,
    public displayName: string = null,
    public oneShot = false,
    public bookType: BookType = null
  ) {
    this.id = null;
    this.seriesBookCount = 0;
    this.displayName = displayName === null ? this.name : displayName;
    this.oneShot = oneShot
  }

  static fromSeriesSearch(seriesSearch: string): Series {
    return new SeriesImpl(seriesSearch);
  }

  static fromSeries(series: Series): SeriesImpl {
    const seriesImpl = new SeriesImpl(series.name, series.editor, series.displayName, series.oneShot, series.bookType);
    seriesImpl.seriesBookCount = series.seriesBookCount;
    seriesImpl.id = series.id;
    return seriesImpl;
  }
}
