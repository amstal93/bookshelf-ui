import {BookImpl} from './book-impl';
import {EditorImpl} from './editor-impl';
import {SeriesImpl} from './series-impl';
import {ArtistImpl} from './artist-impl';
import {Book} from '../book';
import {ContractImpl} from './contract-impl';
import StatusEnum = Book.StatusEnum;

describe('BookImpl', () => {
  test('should create an instance', () => {
    expect(new BookImpl()).toBeTruthy();
  });
  test('fromBook', () => {
    const series = new SeriesImpl('name', null, 'displayName');
    series.id = 34;
    expect(BookImpl.fromBook({
      arkId: 'arkId',
      contracts: [{artists: [new ArtistImpl('authorName', [], 1)], role: {name: 'role1'}}],
      collection: 'collection',
      cover: 'cover',
      editor: {name: 'editorName'},
      isbn: 'isbn',
      series: {
        id: 34,
        seriesBookCount: 0,
        oneShot: false,
        name: 'name', displayName: 'displayName'
      },
      status: undefined,
      title: 'title',
      tome: 'tome',
      year: 'year',
      metadata: null
    })).toStrictEqual({
      arkId: 'arkId',
      contracts: [new ContractImpl({name: 'role1'}, [new ArtistImpl('authorName',[], 1)])],
      collection: 'collection',
      cover: 'cover',
      editor: new EditorImpl('editorName'),
      isbn: 'isbn',
      series,
      status: StatusEnum.UNREAD,
      title: 'title',
      tome: 'tome',
      year: 'year',
      metadata: null
    });
  });
  test('fromBookSearch', () => {
    const book = new BookImpl();
    book.arkId = 'arkId';
    book.contracts = [
      new ContractImpl({name: 'role1'}, [new ArtistImpl('authorName1'), new ArtistImpl('authorName2')]),
      new ContractImpl({name: 'role2'}, [new ArtistImpl('authorName1')])
    ];
    book.collection = 'collection';
    book.cover = 'cover';
    book.editor = new EditorImpl('editorName');
    book.isbn = 'isbn';
    book.series = new SeriesImpl('name');
    book.status = StatusEnum.UNREAD;
    book.title = 'title';
    book.tome = 'tome';
    book.year = 'year';
    expect(BookImpl.fromBookSearch({
      arkId: 'arkId',
      authors: [{role: ['role1', 'role2'], name: 'authorName1'}, {role: ['role1'], name: 'authorName2'}],
      collection: 'collection',
      cover: 'cover',
      editor: 'editorName',
      isbn: 'isbn',
      series: 'name',
      title: 'title',
      tome: 'tome',
      year: 'year'
    })).toStrictEqual(book);
  });
  test('fromBookSearch no contract', () => {
    const book = new BookImpl();
    book.arkId = 'arkId';
    book.contracts = [];
    book.collection = 'collection';
    book.cover = 'cover';
    book.editor = new EditorImpl('editorName');
    book.isbn = 'isbn';
    book.series = new SeriesImpl('name');
    book.status = StatusEnum.UNREAD;
    book.title = 'title';
    book.tome = 'tome';
    book.year = 'year';
    expect(BookImpl.fromBookSearch({
      arkId: 'arkId',
      authors: null,
      collection: 'collection',
      cover: 'cover',
      editor: 'editorName',
      isbn: 'isbn',
      series: 'name',
      title: 'title',
      tome: 'tome',
      year: 'year'
    })).toStrictEqual(book);
  });
});
