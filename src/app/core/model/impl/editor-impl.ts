import {Editor} from '../editor';
import {idType} from "../rest-entity";

export class EditorImpl implements Editor {

  constructor(
    public name: string = '',
    public id: idType = null
  ) {
  }

  static fromEditor(editor: Editor): EditorImpl {
    return new EditorImpl(editor.name, editor.id);
  }

  static fromEditorSearch(editorSearch: string): EditorImpl {
    return new EditorImpl(editorSearch);
  }
}
