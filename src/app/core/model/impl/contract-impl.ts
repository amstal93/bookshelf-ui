import {Artist} from '../artist';
import {AuthorSearch} from '../../../administration/models/author-search';
import {Role} from '../role';
import {ArtistImpl} from './artist-impl';
import {Contract} from '../contract';

export class ContractImpl implements Contract {
  constructor(
    public role: Role,
    public artists: Artist[] = []
  ) {
  }

  static fromContract(contract: Contract): ContractImpl {
    return new ContractImpl(
      contract.role,
      contract.artists
    );
  }

  static fromAuthorSearch(authorSearch: AuthorSearch): Contract[] {
    return authorSearch.role.map(role => new ContractImpl({name: role}, [new ArtistImpl(authorSearch.name)]));
  }
}
