import {ContractImpl} from './contract-impl';
import {ArtistImpl} from './artist-impl';

describe('ContractImpl', () => {
  test('should create an instance', () => {
    expect(new ContractImpl({name: 'role1'})).toBeTruthy();
  });

  test('fromContract', () => {
    expect(ContractImpl.fromContract({artists: [{name: 'name', webLinks: [], id: 0}], role: {name: 'role1'}}))
      .toStrictEqual(new ContractImpl({name: 'role1'}, [{name: 'name', webLinks: [], id: 0}]));
  });
  test('fromBookSearch', () => {
    expect(ContractImpl.fromAuthorSearch({name: 'name', role: ['role1']}))
      .toStrictEqual([new ContractImpl({name: 'role1'}, [new ArtistImpl('name')])]);
  });
});
