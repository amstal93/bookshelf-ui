import {Artist} from '../artist';
import {AuthorSearch} from '../../../administration/models/author-search';
import {WebLink} from '../web-link';
import {idType} from "../rest-entity";

export class ArtistImpl implements Artist {
  constructor(
    public name: string = '',
    public webLinks: WebLink[] = [],
    public id: idType = null
  ) {
  }

  static fromArtist(artist: Artist): ArtistImpl {
    return new ArtistImpl(
      artist.name,
      artist.webLinks,
      artist.id
    );
  }

  static fromAuthorSearch(authorSearch: AuthorSearch): Artist {
    return new ArtistImpl(authorSearch.name);
  }
}
