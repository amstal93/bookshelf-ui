import {RestNamedEntity} from "./rest-named-entity";

export interface Role extends RestNamedEntity {
  name: string;
}
