import {Artist} from './artist';
import {Role} from './role';

export interface Contract {
  artists: Artist[];
  role: Role;
}
