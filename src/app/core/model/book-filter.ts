
export interface BookFilter {
  status: string[];
  groups: string[];
  series: string[];
  bookTypes: string[];
}
