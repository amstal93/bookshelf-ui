import {Editor} from './editor';
import {Series} from './series';
import {RestEntity} from './rest-entity';

export interface MinimalBook extends RestEntity {
  editor?: Editor;
  isbn: string;
  series?: Series;
  title: string;
  tome?: string;
}
