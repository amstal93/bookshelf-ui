import {Loan} from "./loan";
import {RestNamedEntity} from "./rest-named-entity";

export interface Borrower extends RestNamedEntity {
  name: string;
  currentLoansCount?: number;
  loans?: Loan[];
}
