import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {map} from "rxjs/operators";
import {ConfigService} from "../../services/config.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [`
    .app-toolbar {
      position: fixed;
      top: 0; /* Sets the sticky toolbar to be on top */
      right: 0; /* Sets the sticky toolbar to be on top */
      left: 0; /* Sets the sticky toolbar to be on top */
      z-index: 1000;
    }

    .panelClass {
      width: 25%;
    }

    .mat-icon {
      transform: scale(2);
    }
  `]
})
export class HeaderComponent {

  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

  get urlScanner(): Observable<any> {
    return this.configService.appConfig$.pipe(map(c => c?.urlScanner))
  }

  constructor(
    private oidcSecurityService: OidcSecurityService,
    private configService: ConfigService
  ) {
  }

  login() {
    this.oidcSecurityService.authorize();
  }

  signOut() {
    this.oidcSecurityService.logoff();
  }

}
