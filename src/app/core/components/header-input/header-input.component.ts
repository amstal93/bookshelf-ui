import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {map} from "rxjs/operators";

@Component({
  selector: 'app-header-input',
  template: `
    <form [formGroup]="form" (ngSubmit)="onSubmit()">
      <mat-form-field fxFlex style="font-size: 14px" appearance="legacy">
        <mat-label>{{'HEADER.ACTIONS.SEARCH' | translate}}</mat-label>
        <input fxFlex #search matInput formControlName="search" title="search">
        <button mat-icon-button matSuffix *ngIf="form.dirty && form.valid && isAuthenticated$ | async">
          <fa-icon [icon]="['fas', 'search']"></fa-icon>
        </button>
      </mat-form-field>
    </form>
  `
})
export class HeaderInputComponent {

  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

  @ViewChild('search') search: ElementRef;

  form = this.fb.group({
    search: this.fb.control(
      null,
      [Validators.pattern(/^[\d]{13}$/)])
  });

  constructor(
    private oidcSecurityService: OidcSecurityService,
    private fb: FormBuilder,
    private router: Router
  ) {
  }

  onSubmit() {
    if (this.form.valid) {
      this.router.navigate(['administration', 'book', 'add', this.form.value.search])
        // TODO fix error state
        .then(() => {
          this.form.reset();
          this.form.markAsUntouched();
          this.search.nativeElement.blur();
        });
      // TODO add error management
    }
  }
}
