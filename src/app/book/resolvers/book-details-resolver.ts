import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BookService} from '../services/book.service';
import {Observable} from 'rxjs';
import {filter, map} from "rxjs/operators";
import {BookToDisplay} from "../models/book-to-display";

@Injectable({
  providedIn: 'root'
})
export class BookDetailsResolver implements Resolve<BookToDisplay> {

  constructor(
    private bookService: BookService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BookToDisplay> {
    return this.bookService.getById(route.params.isbn).pipe(
      filter(data => data !== null && data !== undefined),
      map(book => {
        return {
          book: book,
          asNext: this.bookService.getNextIsbn(book.isbn),
          asPrevious: this.bookService.getPreviousIsbn(book.isbn)
        };
      }),
    );
  }
}
