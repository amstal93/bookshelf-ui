import {TestBed} from '@angular/core/testing';
import {BookListResolver} from './book-list-resolver';
import {BookService} from "../services/book.service";
import {bookServiceMock} from "../services/__mocks__/book.service";


describe('BookListResolver', () => {
  let service: BookListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: BookService, useValue: bookServiceMock}
      ]
    });
    service = TestBed.inject(BookListResolver);
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(service).toBeTruthy();
    });
  });
});
