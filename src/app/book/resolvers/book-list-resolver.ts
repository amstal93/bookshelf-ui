import {Injectable} from '@angular/core';
import {Book} from "../../core/model/book";
import {BookService} from "../services/book.service";
import {ListResolver} from "../../shared/resolvers/list-resolver";

@Injectable({
  providedIn: 'root'
})
export class BookListResolver  extends ListResolver<Book, BookService> {

  constructor(
    service: BookService
  ) {
    super(service, 100);
  }
}
