import {Book} from "../../core/model/book";

export interface BookToDisplay {
  book: Book | null;
  asNext: string | null;
  asPrevious: string | null;
}
