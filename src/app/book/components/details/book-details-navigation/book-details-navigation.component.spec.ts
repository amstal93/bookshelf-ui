import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookDetailsNavigationComponent} from './book-details-navigation.component';
import {NgxTranslateTestingModule} from "../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {FontAwesomeTestingModule} from "@fortawesome/angular-fontawesome/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {MatButtonModule} from "@angular/material/button";

describe('BookDetailsNavigationComponent', () => {
  let component: BookDetailsNavigationComponent;
  let fixture: ComponentFixture<BookDetailsNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookDetailsNavigationComponent ],
      imports: [
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        RouterTestingModule,
        MatButtonModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDetailsNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });
});
