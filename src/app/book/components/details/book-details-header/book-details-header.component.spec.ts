import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BookDetailsHeaderComponent} from './book-details-header.component';
import {MockTitleDisplayComponent} from "../../../../shared/title-display/__mocks__/title-display.component";
import {MockBookStatusComponent} from "../../../../shared/component/book-status/__mocks__/book-status.component";
import {
  MockBookDetailsNavigationComponent
} from "../book-details-navigation/__mocks__/book-details-navigation.component";
import {BookImpl} from "../../../../core/model/impl/book-impl";

describe('BookDetailsHeaderComponent', () => {
  let component: BookDetailsHeaderComponent;
  let fixture: ComponentFixture<BookDetailsHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookDetailsHeaderComponent, MockTitleDisplayComponent, MockBookStatusComponent, MockBookDetailsNavigationComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDetailsHeaderComponent);
    component = fixture.componentInstance;
    component.book = new BookImpl()
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });
});
