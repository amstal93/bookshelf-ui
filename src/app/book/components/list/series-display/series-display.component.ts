import {Component, Input, ViewChild, ViewEncapsulation} from '@angular/core';
import {SeriesInfo} from '../../../../core/model/series-by-group-container';
import {Book} from '../../../../core/model/book';
import {MatExpansionPanel} from '@angular/material/expansion';
import {MatListOption} from '@angular/material/list';
import {BookService} from '../../../services/book.service';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {Observable} from 'rxjs';
import {filter, map, tap} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {LendModalComponent} from "../../../../shared/component/lend-modal/lend-modal.component";
import {TranslateService} from "@ngx-translate/core";
import {NotificationService} from "../../../../shared/services/notification.service";
import BookStatusEnum = Book.StatusEnum;

type EnumValues = keyof typeof BookStatusEnum;

@Component({
  selector: 'app-series-display',
  templateUrl: './series-display.component.html',
  styleUrls: ['./series-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SeriesDisplayComponent {

  display = false;

  @ViewChild(MatExpansionPanel) matExpansionPanel!: MatExpansionPanel;

  @Input()
  series: string | null = null;

  @Input()
  seriesData: SeriesInfo = {seriesBookCount: 0, books: [], oneShot: false};

  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

  get statusValues(): EnumValues[] {
    return Book.BookStatus.values()
  }

  constructor(
    public dialog: MatDialog,
    private oidcSecurityService: OidcSecurityService,
    private bookService: BookService,
    private translateService: TranslateService,
    private notificationService: NotificationService,
  ) {
  }

  getReadedBooksCount() {
    return this.seriesData.books.filter(b => b.status === 'READ').length;
  }

  getProgressValue(): number {
    if (this.seriesData.seriesBookCount) {
      return (this.seriesData.books.filter(b => b.status === 'READ').length * 100) / this.seriesData.seriesBookCount;
    }
    return 0;
  }

  getPanelState(): boolean {
    return this.matExpansionPanel.expanded;
  }

  changeBookState(selected: MatListOption[], newState: EnumValues) {
    this.bookService.bulkPatch(selected.map(matOption => {
      const book: Book = Object.assign({}, matOption.value);
      book.status = newState;
      return book;
    })).subscribe();
  }

  displayProgress(): boolean {
    return !this.seriesData.oneShot && this.seriesData.seriesBookCount > 0;
  }

  displayBooks($event: boolean) {
    if ($event) {
      this.display = true
    }
  }

  hideBooks() {
    this.display = false
  }

  openLendModal(selectedOption: MatListOption) {
    this.dialog
      .open<LendModalComponent, boolean>(LendModalComponent, {
        data: selectedOption.value,
        width: '500px'
      })
      .afterClosed()
      .pipe(filter(res => res !== 'CANCEL'))
      .pipe(tap(res => console.log(res)))
      .subscribe(({ state, name, title}: { state: string, name: string, title: string }) =>
        this.notificationService.displayMessage(`LOAN.CREATION.NOTIFICATION.${state}`, {name, title})
      );
  }
}
