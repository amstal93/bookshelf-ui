import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SeriesDisplayComponent} from './series-display.component';
import {FontAwesomeTestingModule} from '@fortawesome/angular-fontawesome/testing';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule, MatListOption} from '@angular/material/list';
import {bookServiceMock} from '../../../services/__mocks__/book.service';
import {BookService} from '../../../services/book.service';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {RouterTestingModule} from '@angular/router/testing';
import {
  AngularAuthOidcClientTestingModule
} from '../../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module';
import {MatDialogModule} from "@angular/material/dialog";
import {of} from "rxjs";
import {LendModalComponent} from "../../../../shared/component/lend-modal/lend-modal.component";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import spyOn = jest.spyOn;

class MatListOptionMock extends MatListOption {
  constructor(private v = null)  {
    super(undefined, undefined, undefined);
  }

  get value() {
    return this.v;
  }
}

describe('SeriesDisplayComponent', () => {
  let component: SeriesDisplayComponent;
  let fixture: ComponentFixture<SeriesDisplayComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SeriesDisplayComponent],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        NgxTranslateTestingModule,
        FontAwesomeTestingModule,
        MatExpansionModule,
        MatProgressBarModule,
        MatMenuModule,
        MatDialogModule,
        AngularAuthOidcClientTestingModule,
        MatListModule,
        MatSnackBarModule
      ],
      providers: [
        {provide: BookService, useValue: bookServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('get enum values', waitForAsync(() => {
      jest.clearAllMocks();
      expect(component.statusValues).toStrictEqual(['READ', 'UNREAD', 'READING'])
    }));
    test('should return the number of read books', waitForAsync(() => {
      // return this.seriesData.books.filter(b => b.status === 'READ').length;
      component.seriesData.books.push({
        title: 'title1',
        editor: {name: 'editor', id: undefined},
        contracts: [],
        series: null,
        status: 'READ',
        metadata: null
      });
      component.seriesData.books.push({
        title: 'title2',
        editor: {name: 'editor', id: undefined},
        contracts: [],
        series: null,
        status: 'READING',
        metadata: null
      });
      component.seriesData.books.push({
        title: 'title3',
        editor: {name: 'editor', id: undefined},
        contracts: [],
        series: null,
        status: 'UNREAD',
        metadata: null
      });
      expect(component.getReadedBooksCount()).toStrictEqual(1);
    }));
    test('should return 0', waitForAsync(() => {
      component.seriesData.seriesBookCount = null;
      expect(component.getProgressValue()).toStrictEqual(0);
    }));
    test('should return % of of read books', waitForAsync(() => {
      // return this.seriesData.books.filter(b => b.status === 'READ').length;
      component.seriesData.seriesBookCount = 2;
      component.seriesData.books.push({
        title: 'title1',
        editor: {name: 'editor', id: undefined},
        contracts: [],
        series: null,
        status: 'READ',
        metadata: null
      });
      component.seriesData.books.push({
        title: 'title2',
        editor: {name: 'editor', id: undefined},
        contracts: [],
        series: null,
        status: 'READING',
        metadata: null
      });
      expect(component.getProgressValue()).toStrictEqual(50);
    }));
    test('should return panel state', waitForAsync(() => {
      const spy = spyOn(component[`matExpansionPanel`], `expanded`, 'get').mockImplementation(() => true);
      expect(component.getPanelState()).toBeTruthy();
      expect(spy).toHaveBeenCalledTimes(1);
    }));
    test('should call update state for each selected book', waitForAsync(() => {
      const b1 = {
        title: 'title1',
        editor: {name: 'editor', id: undefined},
        bookType: 'BD',
        contracts: [],
        series: null,
        status: 'READ'
      };

      component.changeBookState([new MatListOptionMock(b1)], 'UNREAD');
      expect(bookServiceMock.bulkPatch).toHaveBeenNthCalledWith(1, [{...b1, status: 'UNREAD'}]);
    }));
    test('should set true to display', waitForAsync(() => {
      component.displayBooks(true);
      expect(component.display).toBeTruthy();
    }));
    test('should not change display', waitForAsync(() => {
      component.displayBooks(true);
      component.displayBooks(false);
      expect(component.display).toBeTruthy();
    }));
    test('should not change display', waitForAsync(() => {
      component.displayBooks(true);
      component.hideBooks();
      expect(component.display).toBeFalsy();
    }));
    test('open lend modal', waitForAsync(() => {
      const matListOption = new MatListOption(undefined, undefined, undefined)
      matListOption[`_value`] = {test: 'toto'}
      const spyDialog = jest.spyOn<any, any>(component[`dialog`], `open`).mockImplementation(() => ({afterClosed: () => of(false)}));
      component.openLendModal(matListOption);
      expect(spyDialog).toHaveBeenNthCalledWith(1, LendModalComponent, {
        data: {test: 'toto'},
        width: '500px'
      });
    }));
  });
});
