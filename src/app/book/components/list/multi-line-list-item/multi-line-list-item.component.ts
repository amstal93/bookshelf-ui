import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-multi-line-list-item',
  template: `
    <mat-list-item class="'book-details-list-item'">
      <div fxFlex="100" fxLayout="column" fxLayoutAlign="end end">
        <h4 mat-line>
          <strong>{{"BOOK.LIST.DETAILS." + label | translate | uppercase}}</strong>
        </h4>
        <span mat-line> {{value}}</span>
      </div>
    </mat-list-item>
  `,
  styles: [`
    h4 {
      margin: 0;
    }
    span {
      opacity: 0.5;
    }
  `]
})
export class MultiLineListItemComponent {

  @Input() titleClass = 'mat-h1';
  @Input() label: string = null;
  @Input() value: string = null;

}
