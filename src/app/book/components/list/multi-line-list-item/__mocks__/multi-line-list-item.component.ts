import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-multi-line-list-item',
  template: `mock app-multi-line-list-item`
})
export class MockMultiLineListItemComponent {

  @Input() titleClass: any;
  @Input() label: any;
  @Input() value: any;

}
