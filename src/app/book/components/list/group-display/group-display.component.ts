import {AfterViewChecked, ChangeDetectorRef, Component, Input, ViewChild, ViewChildren} from '@angular/core';
import {BookBySeriesContainer} from '../../../../core/model/series-by-group-container';
import {Utils} from '../../../../shared/utils';
import {BehaviorSubject, Observable} from 'rxjs';
import {MatAccordion} from '@angular/material/expansion';
import {SeriesDisplayComponent} from '../series-display/series-display.component';

@Component({
  selector: 'app-group-display',
  template: `
    <div fxLayout.gt-md="row" fxLayout.lt-lg="column" class="editor-wrapper" fxLayoutAlign="space-between">
      <div fxFlex.gt-md="15" fxLayout.gt-md="column" fxLayoutGap.lt-lg="1rem" fxLayoutAlign.gt-md="start" fxLayoutAlign.lt-lg="space-between" class="editor-title">
        <h2 fxLayoutAlign="end">{{groupName}}</h2>
        <div fxLayoutAlign="end">
          <button mat-button *ngIf="!isAllPanelOpened()" (click)="accordion.openAll()">
            {{"BOOK.LIST.GROUP.EXPAND_ALL" | translate}}
            <fa-icon [icon]="['fas', 'caret-down']"></fa-icon>
          </button>
          <button mat-button *ngIf="isAllPanelOpened()" (click)="accordion.closeAll()">
            {{"BOOK.LIST.GROUP.COLLAPSE_ALL" | translate}}
            <fa-icon [icon]="['fas', 'caret-up']"></fa-icon>
          </button>
        </div>
      </div>
      <div fxFlex.gt-md="80" fxFlex.lt-lg="100">
        <mat-accordion multi>
          <app-series-display
            *ngFor="let series of (seriesList$|async)"
            [series]="series"
            [seriesData]="(booksBySeries$|async).get(series)">
          </app-series-display>
        </mat-accordion>
      </div>
    </div>
  `,
  styles: [`
    .editor-wrapper {
      margin: 30px 0 20px;
    }

    .editor-wrapper h2 {
      margin: 0;
    }

    .editor-wrapper .editor-title {
      margin-bottom: 5px;
    }
  `]
})
export class GroupDisplayComponent implements AfterViewChecked {

  @ViewChild(MatAccordion) accordion!: MatAccordion;
  @ViewChildren(SeriesDisplayComponent) series: SeriesDisplayComponent[] = [];

  @Input()
  groupName: string | null = null;

  constructor(
    private cdRef: ChangeDetectorRef
  ) {
  }

  private seriesList: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  get seriesList$(): Observable<string[]> {
    return this.seriesList.asObservable();
  }

  private booksBySeries: BehaviorSubject<BookBySeriesContainer> = new BehaviorSubject<BookBySeriesContainer>(new Map());

  get booksBySeries$(): Observable<BookBySeriesContainer> {
    return this.booksBySeries.asObservable();
  }

  @Input()
  set seriesContainer(series: BookBySeriesContainer) {
    if (series !== null) {
      this.seriesList.next(Utils.getMapKeysAsArray(series));
      this.booksBySeries.next(series);
    }
  }

  isAllPanelOpened() {
    return this.series
      .map(panel => panel.getPanelState())
      .reduce((previous, current) => previous && current, true);
  }

  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }
}
