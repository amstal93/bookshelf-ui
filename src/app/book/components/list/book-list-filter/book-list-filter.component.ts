import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {BookFilterService} from "../../../services/book-filter.service";

@Component({
  selector: 'app-book-list-filter',
  template: `
    <form [formGroup]="form" fxFlex fxLayout="column">
    <div>
      <h2>
        {{"BOOK.LIST.FILTER.TITLE" | translate}}
      </h2>
    </div>
    <div fxLayout="column">
      <mat-form-field>
        <label>
          <input matInput formControlName="globalTextCtrl" placeholder="{{'BOOK.LIST.FILTER.FILTER' | translate}}">
        </label>
      </mat-form-field>

      <mat-form-field>
        <mat-label *ngIf="groupByEditors | async">
          {{"BOOK.LIST.FILTER.EDITOR" | translate}}
        </mat-label>
        <mat-label *ngIf="(groupByEditors | async) === false">
          {{"BOOK.LIST.FILTER.INITIAL" | translate}}
        </mat-label>
        <mat-select formControlName="groups" multiple>
          <mat-option *ngFor="let group of groups" [value]="group">{{group}}</mat-option>
        </mat-select>
      </mat-form-field>

      <mat-form-field>
        <mat-label>
          {{"BOOK.LIST.FILTER.SERIES" | translate}}
        </mat-label>
        <mat-select formControlName="series" multiple>
            <mat-optgroup *ngFor="let group of groups" [label]="group">
              <mat-option *ngFor="let seriesName of series" [value]="seriesName">
                {{seriesName}}
              </mat-option>
            </mat-optgroup>
        </mat-select>
      </mat-form-field>
    </div>
    <div fxLayout="row" fxLayoutAlign="space-between">
      <button fxFlex mat-flat-button color="warn" (click)="clearFilters()">
        {{"BOOK.LIST.FILTER.CLEAR" | translate}}
      </button>
    </div>
  </form>
  `
})
export class BookListFilterComponent implements OnInit{

  form: FormGroup = this.fb.group({
    globalTextCtrl: this.fb.control({value: '', disabled: true}),
    groups: [],
    series: []
  });
  groups: string[] = [];
  series: string[] = [];

  groupByEditors: Observable<boolean> = this.bookFilterService.groupByEditors$;

  constructor(
    private fb: FormBuilder,
    private bookFilterService: BookFilterService
  ) {
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(value => this.bookFilterService.updateFilter(value));
  }

  clearFilters() {
    this.form.reset({
      groups: [],
      series: []
    });
  }
}
