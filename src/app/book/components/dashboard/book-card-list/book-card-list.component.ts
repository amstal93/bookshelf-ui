import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Book} from '../../../../core/model/book';

@Component({
  selector: 'app-book-card-list',
  template: `
    <mat-card>
      <mat-card-header>
        <h1>{{title}}</h1>
      </mat-card-header>
      <mat-card-content>
        <div fxLayout="column">
          <app-book-card
            *ngFor="let book of list; trackBy:trackBy"
            [book]="book"
            (readBook)="onReadBook($event)"
            fxFlex>
          </app-book-card>
        </div>
      </mat-card-content>
      <mat-action-row>
        <div fxFlex fxLayoutAlign="end" fxLayoutGap="10px">
          <button mat-button (click)="getNextPage()" [disabled]="nextPageDisabled">
            {{'DASHBOARD.BOOK_LIST.BUTTONS.DISPLAY_MORE'| translate}}
          </button>
        </div>
      </mat-action-row>
    </mat-card>
  `
})
export class BookCardListComponent {
  @Input() title: string;
  @Input() list: Book[] = [];
  @Input() nextPageDisabled = false;
  @Output() nextPage: EventEmitter<void> = new EventEmitter<void>();
  @Output() readBook: EventEmitter<string> = new EventEmitter<string>();

  trackBy(index: number, book: Book){
    return book.isbn
  }

  getNextPage() {
    this.nextPage.emit();
  }

  onReadBook(isbn: string) {
    this.readBook.emit(isbn)
  }
}
