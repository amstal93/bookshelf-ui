import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookCardListComponent} from './book-card-list.component';
import {MatCardModule} from '@angular/material/card';
import {MockBookCardComponent} from '../book-card/__mocks__/book-card.component';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MatExpansionModule} from '@angular/material/expansion';
import {BookImpl} from "../../../../core/model/impl/book-impl";

describe('BookCardListComponent', () => {
  let component: BookCardListComponent;
  let fixture: ComponentFixture<BookCardListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BookCardListComponent, MockBookCardComponent ],
      imports: [
        MatCardModule,
        MatExpansionModule,
        NgxTranslateTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('emit book isbn', () => {
      const spy = jest.spyOn(component.readBook, 'emit').mockImplementation(() => ({}));
      component.onReadBook('12345789456');
      expect(spy).toHaveBeenNthCalledWith(1, '12345789456');
    });

    test('getNextPage', () => {
      jest.clearAllMocks();
      const spy = jest.spyOn(component.nextPage, 'emit').mockImplementation(() => ({}));
      component.getNextPage();
      expect(spy).toHaveBeenNthCalledWith(1);
    });

    test('trackBy', () => {
      jest.clearAllMocks();
      expect(component.trackBy(2, {...(new BookImpl()), isbn: 'testtest'})).toStrictEqual('testtest');
    });
  });
});
