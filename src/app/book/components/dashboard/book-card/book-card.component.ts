import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Book} from '../../../../core/model/book';
import {DisplayImage} from '../../../../shared/display-image';
import {Image} from '../../../../shared/models/image';
import {Router} from '@angular/router';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {OidcSecurityService} from "angular-auth-oidc-client";

@Component({
  selector: 'app-book-card',
  template: `
    <div fxLayout="column" *ngIf="book">
      <div fxLayout="row">
        <div fxFlex="40" class="image-wrapper">
          <app-loader-img
            fxLayoutAlign="center center"
            [imgContainerClass]="'cover'"
            [img]="img">
          </app-loader-img>
        </div>
        <div fxFlex fxLayout="column" fxLayoutAlign="space-between">
          <h3 fxLayout="row" fxLayoutAlign="space-between center">
            <span>
              <span style="display: block" (click)="goToDetails()" class="clickable">
                {{book.title}}
                <span *ngIf="book.tome !== null">(T{{book.tome}})</span>
              </span>
              <app-series-name-display [series]="book.series"></app-series-name-display>
            </span>
            <mat-checkbox *ngIf="isAuthenticated$ | async" disableRipple (change)="onCheck($event)"></mat-checkbox>
          </h3>
        </div>
      </div>
      <mat-divider inset></mat-divider>
    </div>
  `,
  styles: [`
    .image-wrapper {
      padding-bottom: 0.5rem
    }

    mat-checkbox ::ng-deep .mat-checkbox-inner-container {
      width: 40px;
      height: 40px;
    }

    mat-divider {
      padding-bottom: 0.25rem;
      padding-top: 0.25rem
    }
  `]
})
export class BookCardComponent extends DisplayImage implements OnInit {
  isAuthenticated$: Observable<boolean> = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));

  @Input() book: Book

  public img: Image;

  @Output() readBook: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private oidcSecurityService: OidcSecurityService,
    private router: Router
  ) {
    super('/files/covers');
  }

  ngOnInit(): void {
    this.img = this.book ? this.getImg(this.book.cover) : null;
  }

  goToDetails() {
    this.router.navigate(['/', 'book', 'list', 'detail', this.book.isbn]);
  }

  onCheck(event: MatCheckboxChange) {
    if (event.checked) {
      this.readBook.emit(this.book.isbn)
    }
  }
}
