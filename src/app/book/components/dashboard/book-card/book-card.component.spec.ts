import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookCardComponent} from './book-card.component';
import {MatCardModule} from '@angular/material/card';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {MockLoaderImgComponent} from '../../../../shared/loader-img/__mocks__/loader-img.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {RouterTestingModule} from '@angular/router/testing';
import {Book} from '../../../../core/model/book';
import {
  MockSeriesNameDisplayComponent
} from "../../../../shared/component/series-name-display/__mocks__/series-name-display.component";
import {
  AngularAuthOidcClientTestingModule
} from "../../../../../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module";
import spyOn = jest.spyOn;

const bookMock: Book = {
  isbn: 'isbn',
  arkId: 'arkId',
  collection: 'collection',
  cover: 'coverName',
  tome: '1',
  year: '45678',
  title: 'title',
  editor: {name: 'name'},
  series: {displayName: 'displayName', seriesBookCount: 0, name: 'name', oneShot: true,
    bookType: {name: 'bookType', id: 89}},
  contracts: [],
  status: 'UNREAD',
  metadata: null
};

describe('BookCardComponent', () => {
  let component: BookCardComponent;
  let fixture: ComponentFixture<BookCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BookCardComponent, MockLoaderImgComponent, MockSeriesNameDisplayComponent],
      imports: [
        AngularAuthOidcClientTestingModule,
        RouterTestingModule,
        NgxTranslateTestingModule,
        MatCardModule,
        MatExpansionModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });

    test('should init without book', () => {
      expect(component.img).toBeNull();
    });

    test('should init with book', () => {
      component.book = bookMock;
      component.ngOnInit();
      expect(component.img.alt).toStrictEqual('coverName');
      expect(component.img.src.startsWith('/files/covers/coverName')).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('emit book isbn', () => {
      component.book = bookMock
      const spy = jest.spyOn(component.readBook, 'emit').mockImplementation(() => ({}));
      component.onCheck({checked: true, source: undefined});
      expect(spy).toHaveBeenNthCalledWith(1, 'isbn');
    });
    test('should go to details', () => {
      component.book = bookMock;
      const spy = spyOn(component[`router`], 'navigate').mockImplementation(() => Promise.resolve(true));
      component.goToDetails();
      expect(spy).toHaveBeenNthCalledWith(1, ['/', 'book', 'list', 'detail', 'isbn']);
    });
  });
});
