import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ProfileInformationsCardComponent} from './profile-informations-card.component';
import {MatCardModule} from '@angular/material/card';
import {NgxTranslateTestingModule} from '../../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module';
import {ProfileService} from '../../../services/profile.service';
import {profileServiceMock} from '../../../services/__mocks__/profile.service';

describe('ProfileInformationsCardComponent', () => {
  let component: ProfileInformationsCardComponent;
  let fixture: ComponentFixture<ProfileInformationsCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileInformationsCardComponent],
      imports: [
        NgxTranslateTestingModule,
        MatCardModule
      ],
      providers: [
        {provide: ProfileService, useValue: profileServiceMock}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileInformationsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('getValue', () => {
      expect(component.getValue({value: 46, total: 100})).toStrictEqual(54)
    });
    test('getProgressBarValue', () => {
      expect(component.getProgressBarValue({value: 50, total: 100})).toStrictEqual(50)
    });
  });
});
