import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProfileService} from '../../services/profile.service';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {LastAddedBookStoreService} from '../../services/store/last-added-book-store.service';
import {NextBookToReadStoreService} from '../../services/store/next-book-to-read-store.service';
import {map, switchMap} from 'rxjs/operators';
import {forkJoin, Subscription} from "rxjs";
import {BookService} from "../../services/book.service";
import {Book} from "../../../core/model/book";
import StatusEnum = Book.StatusEnum;

@Component({
  selector: 'app-book-dashboard',
  templateUrl: './book-dashboard.component.html'
})
export class BookDashboardComponent implements OnInit, OnDestroy {

  isAuthenticated$ = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));
  lastAddedBooks$ = this.lastAddedBookStore.list$;
  nextBookToRead$ = this.nextBookToReadStore.list$;
  asMoreBookToRead$ = this.nextBookToReadStore.isLastPage$.pipe(map(value => !value));

  public hasData = 0;
  private subscriptions: Subscription[] = [];

  constructor(
    private profileService: ProfileService,
    private bookService: BookService,
    private lastAddedBookStore: LastAddedBookStoreService,
    private nextBookToReadStore: NextBookToReadStoreService,
    private oidcSecurityService: OidcSecurityService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(
      this.profileService.getProfileInformations().subscribe(),
      this.lastAddedBookStore.list$.subscribe(list => this.hasData = list.length)
    );
  }

  moreLastAddedBooks() {
    this.lastAddedBookStore
      .getNextPage(this.lastAddedBookStore.getNextPageNumber())
      .subscribe();
  }

  moreNextBookToRead() {
    this.nextBookToReadStore
      .getNextPage(this.nextBookToReadStore.getNextPageNumber())
      .subscribe();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  updateBook(isbn: string) {
    this.bookService.getById(isbn).pipe(
      switchMap(book => this.bookService.bulkPatch([{...book, status: StatusEnum.READ}])),
      switchMap(() => forkJoin([
          this.profileService.getBookStatus(),
          this.nextBookToReadStore.reloadSameList()
        ])
      ),
    ).subscribe()
  }
}
