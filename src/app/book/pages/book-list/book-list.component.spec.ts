import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListComponent} from './book-list.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MockBookListFilterComponent} from '../../components/list/book-list-filter/__mocks__/book-list-filter.component';
import {MockBookDetailsComponent} from '../book-details/__mocks__/book-details.component';
import {MockGroupDisplayComponent} from '../../components/list/group-display/__mocks__/group-display.component';
import {
  MockBookListActionBarComponent
} from '../../components/list/book-list-action-bar/__mocks__/book-list-action-bar.component';
import {BookService} from '../../services/book.service';
import {bookServiceMock} from '../../services/__mocks__/book.service';
import {RouterTestingModule} from '@angular/router/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {of} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {BookFilterService} from "../../services/book-filter.service";
import {bookFilterServiceMock} from "../../services/__mocks__/book-filter.service";
import {Sort} from "../../../core/model/sort.enum";
import {SortOrder} from "../../../core/model/sort-order.enum";
import {paginationServiceMock} from "../../../shared/services/__mocks__/pagination.service";
import {PaginationService} from "../../../shared/services/pagination.service";
import {scrollEventServiceMocK} from "../../../shared/services/__mocks__/scroll-event.service";
import {ScrollEventService} from "../../../shared/services/scroll-event.service";
import clearAllMocks = jest.clearAllMocks;

const routeMock = {
  snapshot: {data: {resolvedData: {filteredBooks$: of([]), bookType: 'BD'}}},
  data: of({resolvedData: {items: [], pagination: {}}})
};

describe('BookListComponent', () => {
  let component: BookListComponent;
  let fixture: ComponentFixture<BookListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListComponent,
        MockBookListFilterComponent,
        MockBookDetailsComponent,
        MockGroupDisplayComponent,
        MockBookListActionBarComponent
      ],
      imports: [
        RouterTestingModule.withRoutes([]),
        NoopAnimationsModule,
        MatProgressBarModule,
        MatSidenavModule
      ],
      providers: [
        {provide: ActivatedRoute, useValue: routeMock},
        {provide: BookService, useValue: bookServiceMock},
        {provide: BookFilterService, useValue: bookFilterServiceMock},
        {provide: ScrollEventService, useValue: scrollEventServiceMocK},
      ]
    }).overrideComponent(BookListComponent, {
      set: {
        providers: [
          {provide: PaginationService, useValue: paginationServiceMock}
        ]
      }
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should navigate', () => {
      clearAllMocks();
      const spy = jest.spyOn(component[`router`], 'navigate').mockImplementation(() => Promise.resolve(true));

      component.clearBookToDisplay();
      expect(spy).toHaveBeenNthCalledWith(1, ['/', 'book', 'list'], {queryParamsHandling: 'preserve'});
    });
    test('should activate scroll', () => {
      clearAllMocks();
      component.activateScroll();
      expect(scrollEventServiceMocK.toggleScroll).toHaveBeenNthCalledWith(1, true);
    });
    test('should deactivate scroll', () => {
      clearAllMocks();
      component.deactivateScroll();
      expect(scrollEventServiceMocK.toggleScroll).toHaveBeenNthCalledWith(1, false);
    });
    test('should update on filter changes : bookType', waitForAsync(() => {
      clearAllMocks();

      bookFilterServiceMock.filter$.next({
        status: [],
        bookTypes: ['toto', 'tata'],
        series: [],
        groups: []
      })

      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        'toto,tata',
        [],
        [],
        "EDITOR"
      );
    }));
    test('should update on filter changes : series', waitForAsync(() => {
      clearAllMocks();

      bookFilterServiceMock.filter$.next({
        status: [],
        bookTypes: ['toto', 'tata'],
        series: ['toto', 'tata'],
        groups: []
      })

      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        'toto,tata',
        [],
        ['toto', 'tata'],
        "EDITOR"
      );
    }));
    test('should update on filter changes : editors', waitForAsync(() => {
      clearAllMocks();

      bookFilterServiceMock.filter$.next({
        status: null,
        bookTypes: ['toto', 'tata'],
        series: ['toto', 'tata'],
        groups: ['toto', 'tata']
      })

      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        'toto,tata',
        ['toto', 'tata'],
        ['toto', 'tata'],
        "EDITOR"
      );
      expect(bookServiceMock.search).toHaveBeenNthCalledWith(1,
        [], 0, 100, true, true, "ASC", "EDITOR"
      );
    }));
    test('should update on filter changes : all null', waitForAsync(() => {
      clearAllMocks();
      bookFilterServiceMock.filter$.next({
        status: [],
        bookTypes: null,
        series: null,
        groups: null
      })
      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        '',
        [],
        [],
        "EDITOR"
      );
      expect(bookServiceMock.search).toHaveBeenNthCalledWith(1,
        [], 0, 100, true, true, "ASC", "EDITOR"
      );
    }));
    test('should update on filter changes : SortDirection', waitForAsync(() => {
      clearAllMocks();
      bookFilterServiceMock.direction$.next(SortOrder.DESC)
      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        '',
        [],
        [],
        "EDITOR"
      );
      expect(bookServiceMock.search).toHaveBeenNthCalledWith(1,
        [], 0, 100, true, true, "DESC", "EDITOR"
      );
    }));
    test('should update on filter changes : Sort', waitForAsync(() => {
      clearAllMocks();
      const spy = jest.spyOn<any, any>(component[`nextPageLoading`], 'next').mockImplementation(() => {})
      bookFilterServiceMock.sort$.next(Sort.SERIES)
      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        '',
        [],
        [],
        "SERIES"
      );
      expect(bookServiceMock.search).toHaveBeenNthCalledWith(1,
        [], 0, 100, true, true, bookFilterServiceMock.direction$.value, "SERIES"
      );
      expect(spy).toHaveBeenCalledTimes(0)
    }));
    test('should update on next page event', waitForAsync(() => {
      bookFilterServiceMock.filter$.next({
        status: [],
        groups: [],
        series: [],
        bookTypes: []
      })
      bookFilterServiceMock.direction$.next(SortOrder.ASC)
      bookFilterServiceMock.sort$.next(Sort.SERIES)
      clearAllMocks();
      const spy = jest.spyOn<any, any>(component[`nextPageLoading`], 'next').mockImplementation(() => {})
      paginationServiceMock.getNextPage.mockImplementation(() => 2)
      scrollEventServiceMocK.bottomReached$.next()
      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        '',
        [],
        [],
        "SERIES"
      );
      expect(bookServiceMock.search).toHaveBeenNthCalledWith(1,
        [], 2, 100, true, false, bookFilterServiceMock.direction$.value, "SERIES"
      );
      expect(spy).toHaveBeenNthCalledWith(1, true)
      expect(spy).toHaveBeenNthCalledWith(2, false)
    }));
    test('should update on next page event only one call', waitForAsync(() => {
      bookFilterServiceMock.filter$.next({
        status: [],
        groups: [],
        series: [],
        bookTypes: []
      })
      bookFilterServiceMock.direction$.next(SortOrder.ASC)
      bookFilterServiceMock.sort$.next(Sort.SERIES)
      component[`nextPageLoading`].next(true);
      clearAllMocks();
      const spy = jest.spyOn<any, any>(component[`nextPageLoading`], 'next')
      scrollEventServiceMocK.bottomReached$.next()
      expect(bookServiceMock.getSearchParam).toHaveBeenCalledTimes(0);
      expect(bookServiceMock.search).toHaveBeenCalledTimes(0);
      component[`nextPageLoading`].next(false);
      clearAllMocks();
      paginationServiceMock.getNextPage.mockImplementation(() => 4)
      scrollEventServiceMocK.bottomReached$.next();
      expect(bookServiceMock.getSearchParam).toHaveBeenNthCalledWith(1,
        [],
        '',
        [],
        [],
        "SERIES"
      );
      expect(bookServiceMock.search).toHaveBeenNthCalledWith(1,
        [], 4, 100, true, false, bookFilterServiceMock.direction$.value, "SERIES"
      );
      expect(spy).toHaveBeenNthCalledWith(1, true)
      expect(spy).toHaveBeenNthCalledWith(2, false)
    }));
    describe('nextPageLoading', () => {
      test('should emit an event', waitForAsync(() => {
        jest.clearAllMocks();
        component[`nextPageLoading`].next(true);
        component.nextPageLoading$.subscribe(value => expect(value).toBeTruthy());
      }));
    });
  });
});
