import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SortOrder} from '../../core/model/sort-order.enum';
import {Sort} from '../../core/model/sort.enum';
import {debounceTime, map} from 'rxjs/operators';
import {BookFilter} from '../../core/model/book-filter';

@Injectable({
  providedIn: 'root'
})
export class BookFilterService {

  private sort: BehaviorSubject<Sort> = new BehaviorSubject<Sort>(Sort.SERIES);

  get sort$(): Observable<Sort> {
    return this.sort.asObservable();
  }

  private direction: BehaviorSubject<SortOrder> = new BehaviorSubject<SortOrder>(SortOrder.ASC);

  get direction$(): Observable<SortOrder> {
    return this.direction.asObservable();
  }

  private filter: BehaviorSubject<BookFilter> = new BehaviorSubject<BookFilter>({
    status: [],
    series: [],
    groups: [],
    bookTypes: []
  });

  get filter$(): Observable<BookFilter> {
    return this.filter.asObservable().pipe(debounceTime(500));
  }

  public groupByEditors$: Observable<boolean> = this.sort$.pipe(map(sort => sort !== Sort.SERIES));
  public isSortOrderAsc$: Observable<boolean> = this.direction$.pipe(map(direction => direction === SortOrder.ASC));

  changeSortOrder(): void {
    this.direction.next(this.direction.value === SortOrder.ASC ? SortOrder.DESC : SortOrder.ASC)
  }

  changeDisplay(checked: boolean): void {
    if (checked) {
      this.sort.next(Sort.EDITOR)
    } else {
      this.sort.next(Sort.SERIES)
    }
  }

  isGroupByEditor(): boolean {
    return this.sort.value === Sort.EDITOR;
  }

  updateFilter(filter: BookFilter) {
    this.filter.next(filter)
  }

  getCurrentFilters(): { bookFilter: BookFilter; direction: SortOrder; sort: Sort; } {
    return {
      bookFilter: this.filter.value,
      direction: this.direction.value,
      sort: this.sort.value
    };
  }
}
