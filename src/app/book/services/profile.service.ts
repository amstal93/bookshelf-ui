import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../core/services/core.service';
import {catchError, tap} from 'rxjs/operators';
import {Profile} from '../../core/model/profile';
import {ItemStatus} from '../../shared/models/item-status';
import {LastAddedBookStoreService} from './store/last-added-book-store.service';
import {NextBookToReadStoreService} from './store/next-book-to-read-store.service';
import {LibraryCount} from "../../core/model/library-count";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  get libraryCount$(): Observable<LibraryCount> {
    return this.libraryCount.asObservable();
  }

  get bookStatus$(): Observable<ItemStatus> {
    return this.bookStatus.asObservable();
  }

  constructor(
    private http: HttpClient,
    private coreService: CoreService,
    private lastAddBookStore: LastAddedBookStoreService,
    private nextBookToReadStore: NextBookToReadStoreService
  ) {
  }

  private libraryCount: BehaviorSubject<LibraryCount> = new BehaviorSubject<LibraryCount>({
    booksCount: 0,
    bookTypesCount: 0,
    bookTypesBooks: []
  });
  private bookStatus: BehaviorSubject<ItemStatus> = new BehaviorSubject<ItemStatus>({value: 0, total: 0});

  getProfileInformations() {
    this.coreService.updateLoadingState(true);
    return this.http.get<Profile>(`/api/profile`, {observe: 'response'}).pipe(
      tap(res => {
        this.lastAddBookStore.addPage(res.body.lastAddedBook);
        this.nextBookToReadStore.addPage(res.body.nextBookToRead);

        this.bookStatus.next(res.body.booksStatInfo);
        this.libraryCount.next(res.body.libraryCount);

        this.coreService.updateLoadingState(false);
      }),
      catchError(err => {
        console.error('an error occured!', err);
        this.coreService.updateLoadingState(false);
        return throwError(() => new Error(err));
      }));
  }

  getBookStatus() {
    this.coreService.updateLoadingState(true);
    return this.http.get<ItemStatus>(`/api/profile/books/status`).pipe(
      tap(res => {
        this.bookStatus.next(res)
        this.coreService.updateLoadingState(false);
      }),
      catchError(err => {
        console.error('an error occured!', err);
        this.coreService.updateLoadingState(false);
        return throwError(() => new Error(err));
      }));
  }

  getNextBookToReadNextPage(
    page: number = 0,
    size: number = 5,
  ) {
      this.nextBookToReadStore.getNextPage(page, size).subscribe();
  }
}
