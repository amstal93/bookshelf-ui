import fn = jest.fn;
import {Sort} from "../../../core/model/sort.enum";
import {SortOrder} from "../../../core/model/sort-order.enum";
import {BehaviorSubject} from "rxjs";
import {BookFilter} from "../../../core/model/book-filter";

export const bookFilterServiceMock = {
  sort$: new BehaviorSubject<Sort>(Sort.EDITOR),
  direction$: new BehaviorSubject<SortOrder>(SortOrder.ASC),
  filter$: new BehaviorSubject<BookFilter>({
    status: [],
    groups: [],
    series: [],
    bookTypes: []
  }),
  groupByEditors$: new BehaviorSubject<boolean>(false),
  isSortOrderAsc$: new BehaviorSubject<boolean>(true),
  changeSortOrder: fn(),
  changeDisplay: fn(),
  isGroupByEditor: fn(() => true),
  updateFilter: fn(),
  getCurrentFilters: fn(() => ({
    bookFilter: {
      status: [],
      groups: [],
      series: [],
      bookTypes: []
    },
    direction: SortOrder.ASC,
    sort: Sort.SERIES
  }))
};
