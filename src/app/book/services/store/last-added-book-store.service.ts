import {Injectable} from '@angular/core';
import {PageStoreService} from './page-store.service';
import {Book} from '../../../core/model/book';
import {HttpClient} from '@angular/common/http';
import {CoreService} from '../../../core/services/core.service';

@Injectable({
  providedIn: 'root'
})
export class LastAddedBookStoreService extends PageStoreService<Book> {
  constructor(
    http: HttpClient,
    coreService: CoreService
  ) {
    super(http, coreService, '/api/profile/lastAddedBook');
  }
}
