import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {PaginationStatus} from '../../../shared/models/pagination';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Page} from '../../../core/model/page';
import {catchError, tap} from 'rxjs/operators';
import {CoreService} from '../../../core/services/core.service';

@Injectable({
  providedIn: 'root'
})
export class PageStoreService<T> {

  private readonly PAGE_SIZE = 5

  get list$(): Observable<T[]> {
    return this.list.asObservable();
  }

  get isLastPage$(): Observable<boolean> {
    return this.isLastPage.asObservable();
  }

  constructor(
    private http: HttpClient,
    private coreService: CoreService,
    private url
  ) {
  }

  private list: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);

  private isLastPage: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private paginationStatus: PaginationStatus = {currentPage: 0, totalPages: 0, totalElements: 0};

  private getPaginationStatus(page: Page<T>) {
    return {
      currentPage: page.currentPage,
      totalPages: page.totalPages,
      totalElements: page.totalElements
    };
  }

  private updateList(list: T[], paginationStatus: PaginationStatus, concat: boolean = false) {
    if (paginationStatus.currentPage >= (paginationStatus.totalPages - 1)) {
      this.isLastPage.next(true);
    }
    if (concat) {
      const current = this.list.value;
      current.push(...list);
      this.list.next(current);
    } else {
      this.list.next(list);
    }
    this.paginationStatus = paginationStatus;
  }

  public getNextPageNumber() {
    const isLastPage = this.paginationStatus.currentPage === (this.paginationStatus.totalPages - 1);
    return isLastPage ? (this.paginationStatus.totalPages - 1) : this.paginationStatus.currentPage + 1;
  }

  public addPage(page: Page<T>, concat: boolean = false): void {
    this.updateList(page.list, this.getPaginationStatus(page), concat);
  }

  getNextPage(
    page: number = 0,
    size: number = this.PAGE_SIZE,
  ): Observable<Page<T>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', size.toString());

    this.coreService.updateLoadingState(true);
    return this.http.get<Page<T>>(this.url, {params}).pipe(
      tap(itemPage => {
        this.addPage(itemPage, itemPage.currentPage > 0);
        this.coreService.updateLoadingState(false);
      }),
      catchError(err => {
        console.error('an error occured!', err);
        this.coreService.updateLoadingState(false);
        return throwError(() => new Error(err));
      }));
  }

  reloadSameList(): Observable<Page<T>> {
    return this.getNextPage(0, (this.paginationStatus.currentPage + 1) * this.PAGE_SIZE).pipe(
      tap(() => {
        const newPage = (this.list.value.length / this.PAGE_SIZE) - 1
        this.paginationStatus.currentPage = newPage < 0 ? 0 : newPage
      }),
    )
  }
}
