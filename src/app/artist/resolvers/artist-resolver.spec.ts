import {TestBed} from '@angular/core/testing';

import {ArtistResolver} from './artist-resolver';
import {ArtistService} from "../services/artist.service";
import {artistServiceMock} from "../services/__mocks__/artist.service";

describe('ArtistResolver', () => {
  let resolver: ArtistResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: ArtistService, useValue: artistServiceMock}
      ]
    });
    resolver = TestBed.inject(ArtistResolver);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(resolver).toBeTruthy();
    });
  });
});
