import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ArtistService} from "../services/artist.service";
import {Artist} from "../../core/model/artist";

@Injectable({
  providedIn: 'root'
})
export class ArtistResolver implements Resolve<Artist> {

  constructor(
    private artistService: ArtistService
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Artist> {
    return this.artistService.getById(route.params.id);
  }
}
