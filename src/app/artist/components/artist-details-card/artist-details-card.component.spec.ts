import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArtistDetailsCardComponent} from './artist-details-card.component';
import {MatCardModule} from "@angular/material/card";
import {MockWebLinkListComponent} from "../../../shared/web-link-list/__mocks__/web-link-list.component";

describe('ArtistDetailsCardComponent', () => {
  let component: ArtistDetailsCardComponent;
  let fixture: ComponentFixture<ArtistDetailsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtistDetailsCardComponent, MockWebLinkListComponent ],
      imports: [
        MatCardModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistDetailsCardComponent);
    component = fixture.componentInstance;
    component.artist = {id: 0, webLinks: [], name: "toto"};
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(component).toBeTruthy();
    });
  });
});
