import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArtistBookListComponent} from './artist-book-list.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MatListModule} from "@angular/material/list";
import {MockTitleDisplayComponent} from "../../../shared/title-display/__mocks__/title-display.component";

describe('ArtistBookListComponent', () => {
  let component: ArtistBookListComponent;
  let fixture: ComponentFixture<ArtistBookListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtistBookListComponent, MockTitleDisplayComponent ],
      imports: [
        RouterTestingModule,
        MatListModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistBookListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(component).toBeTruthy();
    });
  });
});
