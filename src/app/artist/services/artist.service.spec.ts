import {TestBed} from '@angular/core/testing';

import {ArtistService} from './artist.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('ArtistService', () => {
  let httpTestingController: HttpTestingController;
  let service: ArtistService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ArtistService);
  });

  afterEach(() => httpTestingController.verify());

  describe('Init test', () => {
    test('should be created', () => {
      expect(service[`apiEndpoints`]).toStrictEqual(['artists']);
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    describe('[GET_ALL_BOOKS_BY_ROLE]', () => {
      test('should be the right one', () => {
        service.getAllBooksByRole('id')
          .subscribe((value) => {
            expect(value).toStrictEqual([]);
          });
        const req = httpTestingController.expectOne('/api/artists/id/contracts');
        expect(req.request.method).toStrictEqual('GET');
        req.flush([]);
      });
    });
  });
});
